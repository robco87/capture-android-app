package capturesolution.capturesolutions.Fragments

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import capturesolution.capturesolutions.Activities.LocationDetails
import capturesolution.capturesolutions.Activities.LoginActivity
import capturesolution.capturesolutions.Activities.Selector
import capturesolution.capturesolutions.Objects.Location
import capturesolution.capturesolutions.Objects.User
import capturesolution.capturesolutions.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso

import java.io.File
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LocationListFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LocationListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LocationListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    val GET_FROM_GALLERY = 2
    var locList: ListView? = null
    var locListAdapter: LocListAdapter? = null
    var database: FirebaseDatabase? = null
    private var  isChecked = false
    private var mAuth: FirebaseAuth? = null
    var inflaters: LayoutInflater? = null
    private var locListObject: MutableList<Location>? = mutableListOf<Location>()
    private var authKey: String? = null
    var user: User? = null
    var locImgEdit: ImageView? = null
    var loc: Location? = null
    var locImgAdd: String? = null
    var imgFile: File? = null
    var photoURI: Uri? = null
    val REQUEST_IMAGE_CAPTURE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()

        var user = mAuth?.currentUser
        if (user == null) {
            val intent = Intent(context, Selector::class.java)
            startActivity(intent)
        } else {

        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        inflaters = inflater

        val view = inflater.inflate(R.layout.fragment_location_list, container, false)
        val toolbar = view.findViewById<android.support.v7.widget.Toolbar>(R.id.customtoolbar)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val favsImgButton = view.findViewById<ToggleButton>(R.id.favImgButton)
        val addLocImgButton = view.findViewById<ImageButton>(R.id.addLocImgButton)
        val searchBar = view.findViewById<EditText>(R.id.searchBar)
        locList = view?.findViewById(R.id.locList)
        searchBar.addTextChangedListener(object : TextWatcher {
            //search bar implementation
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.isNotEmpty()) {
                    val list = locListObject?.filter { it.name!!.toLowerCase().contains(p0!!, ignoreCase = true) }//filter object list if name contains entered text
                    locListAdapter = LocListAdapter(context!!, list!!)
                    locList?.adapter = locListAdapter
                } else {
                    locListAdapter = LocListAdapter(context!!, locListObject!!)
                    locList?.adapter = locListAdapter
                }
            }
        })

        favsImgButton?.setOnCheckedChangeListener { buttonView, isChecked ->

            //isChecked = !it.isChecked
            var copyLoc = locListObject

            if(isChecked) {
                var userFav = user?.Favorites
                var list: MutableList<Location>? = mutableListOf<Location>()
                if(userFav!=null){
                    buttonView.background = resources.getDrawable(R.drawable.ic_star_white_24dp)
                    buttonView.background.setTint(resources.getColor(android.R.color.white))
                    for(i in userFav.values.iterator()){
                        val filtered = locListObject?.filter { it.key.equals(i) }//filter object list if name contains entered text
                        list?.addAll(filtered!!)
                    }
                }
                else{
                    Toast.makeText(context, "You do not have any favorite locations", Toast.LENGTH_SHORT).show()
                    list = copyLoc
                }
                locListAdapter = LocListAdapter(context!!, list!!)
            }else{
                locListObject = copyLoc
                buttonView.background = resources.getDrawable(R.drawable.ic_star_border_white_24dp)
                buttonView.background.setTint(resources.getColor(android.R.color.white))
                locListAdapter = LocListAdapter(context!!, locListObject!!)
            }
            locList?.adapter = locListAdapter
        }

        addLocImgButton?.setOnClickListener {
            val builder = AlertDialog.Builder(context!!)
            // Get the layout inflater
            val dialogView = layoutInflater.inflate(R.layout.dialog_edit_location, null)
            builder.setView(dialogView)
            //dialog box views
            locImgEdit = dialogView.findViewById<ImageView>(R.id.locImg)
            val picButton = dialogView.findViewById<Button>(R.id.takeLocPic)
            val nameText = dialogView.findViewById<EditText>(R.id.editName)
            val addText = dialogView.findViewById<EditText>(R.id.editStreetAddress)
            val cityText = dialogView.findViewById<EditText>(R.id.newCity)
            val stateText = dialogView.findViewById<EditText>(R.id.newState)
            val zipText = dialogView.findViewById<EditText>(R.id.newZip)
            val galleryUpload = dialogView.findViewById<ImageButton>(R.id.uploadGallery)
            //when userRole clicks new pic button
            galleryUpload.setOnClickListener {
                if (ContextCompat.checkSelfPermission(context!!,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity!!,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            1)
                } else {
                    startActivityForResult(Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY)
                }
            }
            picButton.setOnClickListener {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
                    imgFile = createImageFile()//create placeholder image file
                    photoURI = FileProvider.getUriForFile(context!!,
                            "capturesolution.capturesolution.fileprovider",
                            imgFile!!)//uri from file
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
            //userRole wishes to save changes
            builder.setPositiveButton("Save", null)
            builder.setNegativeButton("Cancel", null)
            var alertDialog = builder.create()
            alertDialog.setOnShowListener(object : DialogInterface.OnShowListener {
                override fun onShow(p0: DialogInterface?) {
                    var positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    var negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    positiveButton.setOnClickListener {
                        //get new text from EditTexts
                        val newName = nameText.text.toString()
                        val newAddr = addText.text.toString()
                        val newState = stateText.text.toString()
                        val newCity = cityText.text.toString()
                        val newZip = zipText.text.toString()
                        //reference for location object being edited

                        //set new values to each field, regardless of change or not
                        if (newName.isEmpty()) {
                            Toast.makeText(context, "Location Not Saved. Please Fill All Fields", Toast.LENGTH_SHORT).show()
                        } else {
                            val locKey = database?.getReference("Locations")?.child(authKey)?.push()?.key
                            val locRef = database?.getReference("Locations")?.child(authKey)?.child(locKey)
                            var newLocation = Location()
                            newLocation.key = locKey
                            newLocation.address = newAddr
                            newLocation.name = newName
                            newLocation.state = newState
                            newLocation.city = newCity
                            newLocation.zip = newZip
                            //if userRole has taken a new picture, upload it
                            if (photoURI != null) {
                                val storageRef = FirebaseStorage.getInstance().reference.child("LocationPhotos").child(authKey!!)//LocationPhotos within Firebase Storage
                                val riversRef = storageRef.child(photoURI!!.lastPathSegment)//storage ref for new uploaded image
                                val uploadTask = riversRef.putFile(photoURI!!)//uploading image file
                                uploadTask.addOnSuccessListener(object : OnSuccessListener<UploadTask.TaskSnapshot> {
                                    override fun onSuccess(p0: UploadTask.TaskSnapshot?) {
                                        riversRef.downloadUrl.addOnSuccessListener(object : OnSuccessListener<Uri> {
                                            //gettting download url for image from firebase storage
                                            override fun onSuccess(p0: Uri?) {
                                                newLocation.url = p0.toString()
                                                locRef?.setValue(newLocation)
                                            }
                                        })
                                    }
                                })
                                newLocation.url = photoURI.toString()
                                locListObject?.add(newLocation)
                                locListAdapter?.notifyDataSetChanged()
                            } else {
                                newLocation.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                                locRef?.setValue(newLocation)
                                locListObject?.add(newLocation)
                                locListAdapter?.notifyDataSetChanged()
                            }
                            alertDialog.dismiss()
                        }
                    }
                    negativeButton.setOnClickListener {
                        if(photoURI!=null) {
                            var file = File(photoURI?.path)
                            file.delete()
                        }
                        p0!!.cancel()

                        alertDialog.dismiss()
                    }
                }
            })
            alertDialog.show()
        }


        getAccountKey()

        locList?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            //need to implement position -> job site -


            val intent = Intent(context, LocationDetails::class.java)
            intent.putExtra("key", locListAdapter?.getItemKey(position))
            intent.putExtra("authKey", authKey)
            startActivity(intent)
        }
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val exif = ExifInterface(Uri.fromFile(imgFile)?.path)
            if (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 200) == 200) {
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, "6")
                exif.saveAttributes()
            }
            Picasso.get().load(photoURI).fit().into(locImgEdit!!)

        }
        if (requestCode == GET_FROM_GALLERY && resultCode == AppCompatActivity.RESULT_OK) {
            var selectedImage = data?.data
            photoURI = selectedImage
            Picasso.get().load(photoURI).fit().into(locImgEdit!!)
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LocationListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                LocationListFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        /*inflater?.inflate(R.menu.menu_add_new_note, menu)
        if(user?.Role== "basic"){
            menu?.findItem(R.id.add_new)?.setVisible(false)
        }*/
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var copyLoc = locListObject
        if(item?.itemId==R.id.add_new) {


        }
        if(item?.itemId == R.id.favorites){

        }
        return false
    }

    //get account key that matches userRole
    fun getAccountKey() {
        val reference = database?.getReference("Users")
        val loggedUser = FirebaseAuth.getInstance().currentUser
        val queryRef = reference
        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {

                for (ds in p0!!.children) {
                    var duser = ds?.getValue(User::class.java)
                    duser?.id = ds.key

                    if (duser?.id.equals(loggedUser?.uid)) {
                        if(duser?.Role!="deleted") {
                            authKey = duser?.Key
                            user = duser
                            setHasOptionsMenu(true)
                            if (user?.Role?.toLowerCase() == "admin") {
                                locList?.setOnItemLongClickListener { adapterView, view, i, l ->
                                    val dialog = AlertDialog.Builder(context!!)
                                    val delLoc = locListAdapter?.getLocationItem(i)
                                    dialog.setTitle("Delete Location:")
                                    dialog.setMessage(delLoc?.name)
                                    dialog.setPositiveButton("Delete", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {
                                            database?.reference?.child("Locations")?.child(authKey)?.child(delLoc?.key)?.removeValue()
                                        }
                                    })
                                    dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {

                                        }
                                    })
                                    dialog.create().show()
                                    true
                                }

                            }else if(user?.Role?.toLowerCase() == "basic"){
                                //newLoc.visibility = View.GONE

                            }
                            getLocList()
                        }
                        else{
                            Toast.makeText(context, "Your Account has been Deleted. Please contact your Administrator for support.",Toast.LENGTH_LONG).show()
                            loggedUser?.delete()
                            mAuth?.signOut()
                            startActivity(Intent(context, LoginActivity::class.java))
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                for (ds in p0!!.children) {
                    var duser = ds?.getValue(User::class.java)
                    duser?.id = ds.key

                    if (duser?.id.equals(loggedUser?.uid)) {
                        if(duser?.Role!="deleted") {
                            authKey = duser?.Key
                            user = duser
                            if (user?.Role?.toLowerCase() == "admin") {

                                setHasOptionsMenu(true)
                                locList?.setOnItemLongClickListener { adapterView, view, i, l ->
                                    val dialog = AlertDialog.Builder(context!!)
                                    val delLoc = locListAdapter?.getLocationItem(i)
                                    dialog.setTitle("Delete Location:")
                                    dialog.setMessage(delLoc?.name)
                                    dialog.setPositiveButton("Delete", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {
                                            database?.reference?.child("Locations")?.child(authKey)?.child(delLoc?.key)?.removeValue()
                                        }
                                    })
                                    dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {

                                        }
                                    })
                                    dialog.create().show()
                                    true
                                }

                            }else if(user?.Role?.toLowerCase() == "manager"){
                                setHasOptionsMenu(true)
                                //newLoc.visibility = View.GONE
                            }
                            getLocList()
                        }
                        else{
                            Toast.makeText(context, "Your Account has been Deleted. Please contact your Administrator for support.",Toast.LENGTH_LONG).show()
                            loggedUser?.delete()
                            mAuth?.signOut()
                            startActivity(Intent(context, LoginActivity::class.java))
                        }
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                mAuth?.signOut()
                activity?.finish()

            }
        })
    }

    fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());
        val imageFileName = "PNG_" + timeStamp + "_";
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        return image
    }

    private fun centerTitle() {

    }

    //get list of locations that match userRole key
    fun getLocList() {
        val reference = database?.getReference("Locations")
        val queryLoc = reference?.child(authKey)
        queryLoc?.addChildEventListener(object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                replaceLocInList(p0)
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                createLocList(p0)
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                removeLocInList(p0)
            }

            override fun onCancelled(p0: DatabaseError?) {

            }
        })

    }

    fun createLocList(p0: DataSnapshot?) {
        val loc = p0?.getValue(Location::class.java)
        if (locListObject?.find { it.key == loc?.key } == null) {
            locListObject?.add(loc!!)
            if (locListAdapter == null) {
                locListAdapter = LocListAdapter(context!!, locListObject!!)
                locList?.adapter = locListAdapter
            } else {
                locListAdapter?.notifyDataSetChanged()
            }
        }
    }

    fun replaceLocInList(p0: DataSnapshot?) {
        val newLoc = p0?.getValue(Location::class.java)

        var oldLoc = locListObject?.find { it.key == newLoc?.key }//finding the old object by key
        locListObject?.remove(oldLoc)//removing old object
        locListObject?.add(newLoc!!)//adding in new object
        locListAdapter?.notifyDataSetChanged()//refresh adapter
    }

    fun removeLocInList(p0: DataSnapshot?) {
        val loc = p0?.getValue(Location::class.java)
        locListObject?.remove(locListObject?.find { it.key == loc?.key })//finding the removed object in the list
        locListAdapter?.notifyDataSetChanged()//refresh adapter
    }


    class   LocListAdapter(private var context: Context, private var items: List<Location>) : BaseAdapter() {

        private class SiteViewHolder(row: View?) {
            var txtName: TextView? = null
            var txtAddress: TextView? = null
            var locImage: ImageView? = null
            var progBar: ProgressBar? = null

            init {
                this.txtName = row?.findViewById<TextView>(R.id.locName)
                this.txtAddress = row?.findViewById<TextView>(R.id.addressLine)
                this.locImage = row?.findViewById<ImageView>(R.id.locImg)
                this.progBar = row?.findViewById<ProgressBar>(R.id.homeprogress)
            }
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

            val view: View?
            val viewHolder: SiteViewHolder

            if (p1 == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.siterowview, null)
                viewHolder = SiteViewHolder(view)
                view?.tag = viewHolder
            } else {
                view = p1
                viewHolder = view.tag as SiteViewHolder
            }

            val locDetails = getItem(p0) as Location//getting item from read only list

            if (locDetails.url == null && locDetails.localUri == null) {
                locDetails.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
            }
            viewHolder.txtName?.text = locDetails.name
            if (locDetails.address.isNullOrBlank() && locDetails.city.isNullOrBlank()) {
                viewHolder.txtAddress?.text = locDetails.state + " " + locDetails.zip
            }
            if (locDetails.state.isNullOrBlank() && locDetails.zip.isNullOrBlank()) {
                viewHolder.txtAddress?.text = locDetails.address + " " + locDetails.city
            }
            else {
                viewHolder.txtAddress?.text = locDetails.address + " " + locDetails.city + ", " + locDetails.state + " " + locDetails.zip
            }

            /*Picasso.get().load(locDetails.url).fit().into(viewHolder.locImage, object: Callback {
                override fun onSuccess() {
                    viewHolder.progBar?.visibility = View.INVISIBLE
                }

                override fun onError(e: Exception?) {

                }
            })*/
            try {
                Glide.with(viewHolder.locImage?.context).load(locDetails.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        viewHolder.progBar?.visibility = View.INVISIBLE
                        return false
                    }

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return true
                    }
                }).into(viewHolder.locImage)
            }catch (e: IllegalArgumentException){

            }

            return view as View
        }

        fun getItemKey(p0: Int): String? {
            val sortedList = items.sortedBy { it.name}//inline sort alphabetically by object name
            val sortedReadOnly: List<Location> = sortedList
            return sortedReadOnly[p0].key
        }

        fun getLocationItem(p0: Int): Location?{
            val sortedList = items.sortedBy { it.name }//inline sort alphabetically by object name
            val sortedReadOnly: List<Location> = sortedList
            return sortedReadOnly[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }


        override fun getItem(p0: Int): Any {
            val sortedList = items.sortedBy { it.name}//inline sort alphabetically by object name
            val sortedReadOnly: List<Location> = sortedList
            return sortedReadOnly[p0]
        }

        override fun getCount(): Int {
            return items.size
        }

    }


}
