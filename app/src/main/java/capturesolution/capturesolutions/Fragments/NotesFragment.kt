package capturesolution.capturesolutions.Fragments

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageButton
import android.widget.TextView
import capturesolution.capturesolutions.Activities.AddNote
import capturesolution.capturesolutions.Objects.Note
import capturesolution.capturesolutions.Objects.User

import capturesolution.capturesolutions.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import de.cketti.mailto.EmailIntentBuilder
import kotlinx.android.synthetic.main.fragment_notes.*
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NotesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NotesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class NotesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    var noteList: RecyclerView? = null

    var database: FirebaseDatabase? = null
    var authKey: String? = null
    var user: User? = null
    var mAuth: FirebaseAuth? = null
    private var notesListObj: MutableList<Note>? = mutableListOf<Note>()
    var notesAdapter: NotesListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        getAccountKey()

        (activity as AppCompatActivity).supportActionBar?.title =""
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity as AppCompatActivity).supportActionBar?.title =""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_notes, container, false)

        val toolbar = view.findViewById<android.support.v7.widget.Toolbar>(R.id.customtoolbar)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val addNoteImgButton = view.findViewById<ImageButton>(R.id.addNoteImgButton)

        noteList = view.findViewById(R.id.notesList)
        noteList?.layoutManager = LinearLayoutManager(context)


        addNoteImgButton.setOnClickListener {
            var addNote = Intent(context, AddNote::class.java)
            startActivity(addNote)
        }

        return view
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun getAccountKey() {
        val reference = database?.getReference("Users")
        val loggedUser = FirebaseAuth.getInstance().currentUser
        val queryRef = reference
        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {

                for (ds in p0!!.children) {
                    var duser = ds?.getValue(User::class.java)
                    duser?.id = ds.key

                    if (duser?.id.equals(loggedUser?.uid)) {
                        if(duser?.Role!="deleted") {
                            authKey = duser?.Key
                            user = duser
                            getNotesList()
                        }
                        else{

                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot?) {

            }
        })
    }


    fun getNotesList(){
        //Log.d("UserDET",userRole?.Key + "   "+userRole?.id)
        val reference = database?.getReference("Notes")
        val queryNotes = reference?.child(user?.Key)?.child(user?.id)

        queryNotes?.addChildEventListener(object:ChildEventListener{
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                val note = p0?.getValue(Note::class.java)
                if(notesListObj?.find { it.key == note?.key } != null){
                    var oldNote = notesListObj?.find { it.key == note?.key }
                    notesListObj?.remove(oldNote)
                    notesListObj?.add(note!!)
                    //add adapter here
                    if(notesAdapter == null){
                        notesAdapter = NotesListAdapter(notesListObj!!,context!!, authKey!!, user?.id!!)
                        notesList.adapter = notesAdapter
                    }else{
                        notesAdapter?.notifyDataSetChanged()
                    }

                }
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                val note = p0?.getValue(Note::class.java)
                    if(user?.Role=="admin"){
                        notesListObj?.add(note!!)
                    }
                    if(user?.Role == "basic")

                    notesListObj?.sortByDescending { it.date }
                    //add adapter here
                    if(notesAdapter == null){
                        notesAdapter = NotesListAdapter(notesListObj!!,context!!,authKey!!,user?.id!!)
                        notesList.adapter = notesAdapter
                    }else{
                       notesAdapter?.notifyDataSetChanged()
                    }

            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                val note = p0?.getValue(Note::class.java)
                notesListObj?.remove(notesListObj?.find { it.key == note?.key })

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onCancelled(p0: DatabaseError?) {
            }
        })

    }

    class NotesListAdapter (var notes: MutableList<Note>, var context: Context, var authKey: String, var userKey: String):RecyclerView.Adapter<NotesListAdapter.ViewHolder>(){

        fun getNote(pos: Int):Note{
            return notes[pos]
        }

        override fun getItemCount(): Int {
            return notes.size
        }

        override fun getItemId(position: Int): Long {
            return 0
        }


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            val note = getNote(position)
            val formatter = SimpleDateFormat("h:mm a MMMM d, yyyy")
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = note.date!!*1000

            holder.noteTimestampView?.text =  formatter.format(calendar.timeInMillis)
            holder.noteTitleView?.text = note.title

            holder.notesItemBox?.setOnLongClickListener {
                val dialog = AlertDialog.Builder(context)
                dialog.setTitle("Delete Report?")
                dialog.setPositiveButton("Delete", object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        var database = FirebaseDatabase.getInstance()
                        database?.reference?.child("Notes")?.child(authKey)?.child(userKey)?.child(getNote(holder.adapterPosition).key)?.removeValue()
                        notes.removeAt(holder.adapterPosition)
                        notifyDataSetChanged()
                    }
                })
                dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {

                    }
                })
                dialog.create().show()
                true
            }
            holder.notesItemBox?.setOnClickListener {
                val editNoteIntent = Intent(context, AddNote::class.java)
                editNoteIntent.putExtra("editMode",note.key)
                editNoteIntent.putExtra("editNoteTitle",note.title)
                editNoteIntent.putExtra("editNoteContent",note.note)
                startActivity(context,editNoteIntent, null)
            }

            holder.exportNoteButton?.setOnClickListener {
                createEmail(note)
            }



        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.notes_row_item,parent,false)
            return ViewHolder(view)
        }

        override fun getItemViewType(position: Int): Int {
            return 0
        }

        class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
            var noteTitleView: TextView? = null
            var noteTimestampView: TextView? = null
            var exportNoteButton: ImageButton? = null
            var notesItemBox: ConstraintLayout? = null

            init{

                this.noteTitleView = view.findViewById(R.id.noteTitleText)
                this.noteTimestampView = view.findViewById(R.id.noteTimestampText)
                this.exportNoteButton = view.findViewById(R.id.exportNoteButton)
                this.notesItemBox = view.findViewById(R.id.noteListBox)

            }
        }

        fun createEmail(note:Note){
            var sendReport = EmailIntentBuilder.from(context)
            sendReport.subject("Export File" )
            sendReport.body(note.title + "\n" +
                    note.note
            ).start()
        }


    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NotesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                NotesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
