package capturesolution.capturesolutions.Fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import capturesolution.capturesolutions.Activities.MainActivity
import capturesolution.capturesolutions.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_register.*
import com.google.firebase.auth.AuthResult
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.*




/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RegisterFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    var database: FirebaseDatabase? = null
    var mAuth: FirebaseAuth? = null
    private var mListener: OnFragmentInteractionListener? = null
    private var keyList = mutableListOf<String>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_register, container, false)
        var registerConfirm = view.findViewById<Button>(R.id.registerConfirm)
        database = FirebaseDatabase.getInstance()
        registerConfirm.setOnClickListener {

            val email = email.text.toString()
            val name = name.text.toString()
            val pass = pass.text.toString()
            val confirmPass = confirmPass.text.toString()
            val authKey = custKey.text.toString()

            registerConfirm.visibility = View.INVISIBLE
            registerProg.visibility = View.VISIBLE

            if(!email.equals("") && !name.equals("") && !pass.equals("") && !confirmPass.equals("") && !authKey.equals("")){//check if every field is filled out
                if(confirmPass.equals(pass)){//check if both passwords are equal
                    val reference = database?.getReference("Account_Keys")
                    var queryRef = reference
                    queryRef?.addChildEventListener(object : ChildEventListener{//querying into authkey list
                        override fun onCancelled(p0: DatabaseError?) {
                        }
                        override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                        }
                        override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                        }
                        override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                            if(p0?.value=="available") {
                                keyList.add(p0.key!!)
                            }else{

                            }

                        }
                        override fun onChildRemoved(p0: DataSnapshot?) {
                        }

                    })

                    queryRef?.addListenerForSingleValueEvent(object : ValueEventListener{
                        override fun onCancelled(p0: DatabaseError?) {

                        }

                        override fun onDataChange(p0: DataSnapshot?) {
                           //once all the authkeys are added to the list, execute the follow code only once
                                if (keyList.contains(authKey)){//if the provided authkey is in the db, userRole can be registered
                                    val selKey = authKey
                                    mAuth?.createUserWithEmailAndPassword(email,pass)?.addOnCompleteListener(activity!!, object: OnCompleteListener<AuthResult>{//register new userRole w email pass
                                    override fun onComplete(p0: Task<AuthResult>){
                                        if(p0.isSuccessful){//once succesful, add all relevant details to db
                                            val user = mAuth?.currentUser
                                            val userRef = database?.getReference("Users")?.child(authKey)?.child(user?.uid)
                                            database?.getReference("Account_Keys")?.child(selKey)?.setValue("unavailable")
                                            userRef?.child("Email")?.setValue(email)
                                            userRef?.child("Key")?.setValue(selKey)
                                            userRef?.child("Name")?.setValue(name)
                                            userRef?.child("id")?.setValue(user?.uid)
                                            userRef?.child("Role")?.setValue("admin")

                                            val profileUpdates = UserProfileChangeRequest.Builder()
                                                    .setDisplayName(selKey).build()
                                            user?.updateProfile(profileUpdates)

                                            startActivity(Intent(activity, MainActivity::class.java))//open main activity once userRole is registered, signed in
                                        }
                                        else if(!p0.isSuccessful){//if unsuccesful, return some error, make progress bar invisible
                                            registerConfirm.visibility = View.VISIBLE
                                            registerProg.visibility = View.INVISIBLE
                                            Toast.makeText(activity, "There was an Error. Please try again.", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                    })
                                }else{
                                    registerConfirm.visibility = View.VISIBLE
                                    registerProg.visibility = View.INVISIBLE
                                    Toast.makeText(activity, "Customer Key Error. Please contact your administrator.", Toast.LENGTH_SHORT).show()
                                }

                        }
                    })

                }
                else{
                    Toast.makeText(activity, "Passwords Must Match", Toast.LENGTH_SHORT).show()
                    registerConfirm.visibility = View.VISIBLE
                    registerProg.visibility = View.INVISIBLE
                }
            }else{
                Toast.makeText(activity, "Please Complete All Fields", Toast.LENGTH_LONG).show()
                registerConfirm.visibility = View.VISIBLE
                registerProg.visibility = View.INVISIBLE
            }
        }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        database = FirebaseDatabase.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): RegisterFragment {
            val fragment = RegisterFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }


}// Required empty public constructor
