package capturesolution.capturesolutions.Fragments

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat;
import capturesolution.capturesolutions.Activities.SettingItemList
import capturesolution.capturesolutions.Objects.User

import capturesolution.capturesolutions.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase

import android.support.v7.widget.Toolbar
import capturesolution.capturesolutions.Activities.Selector


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Settings.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Settings.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Settings : PreferenceFragmentCompat() {

    private var mAuth: FirebaseAuth? = null
    private var authKey : String? = null
    var database: FirebaseDatabase? = null

    private var name: String? =  null
    private var email: String? = null
    private var role : String? = null


    var namePref: Preference? = null
    var emailPref: Preference? = null
    var rolePref: Preference? = null




    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        addPreferencesFromResource(R.xml.preference_list)

        var toolbar = Toolbar(context)

        var intent = Intent(context, SettingItemList::class.java)
        var logoutPref = findPreference("logout_preference")
        var userPref = findPreference("user_preference")
        var repTypePref = findPreference("repType_preference")
        logoutPref.setOnPreferenceClickListener {
            val dialog = AlertDialog.Builder(context!!)
            dialog.setTitle("Logout")
            dialog.setMessage("Are you sure you want to logout?")
            dialog.setPositiveButton("Logout",object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    mAuth?.signOut()
                    val logoutIntent = Intent(context, Selector::class.java)
                    logoutIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(logoutIntent)
                }
            })
            dialog.setNegativeButton("Cancel",object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                }
            })
            dialog.create().show()
            true
        }
        userPref.setOnPreferenceClickListener {
            intent.putExtra("type", "userList")
            intent.putExtra("authKey",authKey)
            intent.putExtra("userRole", role)
            startActivity(intent)
            true
        }
        repTypePref.setOnPreferenceClickListener {
            intent.putExtra("type", "repTypeList")
            intent.putExtra("authKey",authKey)
            intent.putExtra("userRole",role)
            startActivity(intent)
            true
        }
        getAccountKey()
    }





    fun getAccountKey(){
        namePref = findPreference("name_preference")
        emailPref = findPreference("email_preference")
        rolePref = findPreference("role_preference")
        val reference = database?.getReference("Users")
        val loggedUser = mAuth?.currentUser
        val queryRef = reference

        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {

                for (ds in p0!!.children) {
                    var user = ds?.getValue(User::class.java)
                    user?.id = ds.key

                    if(user?.id.equals(loggedUser?.uid)){
                        authKey = user?.Key
                        name = user?.Name
                        email = user?.Email
                        role = user?.Role

                        namePref?.summary = name
                        emailPref?.summary = email
                        rolePref?.summary = role
                        listView.setPadding(0,0,0,0)
                        if(role?.toLowerCase() == "admin" || role?.toLowerCase() == "manager"){

                        }
                        else{
                            val adminCategory = findPreference("admin_category")
                            val userPref = findPreference("user_preference")
                            val repTypePref = findPreference("repType_preference")
                            adminCategory.isVisible = false
                            userPref.isVisible = false
                            repTypePref.isVisible = false
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot?) {

            }
        })

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Settings.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                Settings().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
