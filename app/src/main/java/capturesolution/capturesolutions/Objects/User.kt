package capturesolution.capturesolutions.Objects

import com.google.firebase.database.*

/**
 * Created by Gautam on 3/12/2018.
 */
class User {
    var id: String? = null
    @get: PropertyName("Email")
    @set: PropertyName("Email")
    var Email: String? = null
    @get: PropertyName("Key")
    @set: PropertyName("Key")
    var Key: String? = null
    @get: PropertyName("Name")
    @set: PropertyName("Name")
    var Name: String? = null
    @get: PropertyName("Role")
    @set: PropertyName("Role")
    var Role: String? = null
    @get: PropertyName("Favorites")
    @set: PropertyName("Favorites")
    var Favorites: Map<String,String>? = null


    constructor() {}

    constructor(Role: String, Email: String, Name: String, Key: String){
        this.Email = Email
        this.Key = Key
        this.Name =Name
        this.Role = Role

    }

    constructor(Role: String, Email: String, Name: String, Key: String, Favorites: Map<String,String>){
        this.Email = Email
        this.Key = Key
        this.Name =Name
        this.Role = Role
        this.Favorites = Favorites

    }



}