package capturesolution.capturesolutions.Objects

import android.net.Uri
import java.io.Serializable

/**
 * Created by Gautam on 3/15/2018.
 */
class DataFile:Serializable {

    var data: String? = null
    var date: Long? = null
    var name: String? = null
    var dbRef:String? = null
    var size: Long? = null
    var type: String? = null
    var url : String? = null
    var note: String? = null
    var title: String? = null
    var metaData: HashMap<String, String>? = null
    var uri: Uri? = null
    var username: String? = null
    var key: String? = null

    constructor() {}

    //note constructor
    constructor(date: Long, name: String, content: String, type: String, userName: String) {
        this.date =date
        this.title = name
        this.note = content
        this.type =type
        this.username = userName
    }
    //photo constructor
    constructor(date: Long, name: String, size: Long, type: String, url: String, userName: String) {
        this.date =date
        this.name = name
        this.size = size
        this.type = type
        this.url = url
        this.username = userName

    }
    //form constructor
    constructor(data: String, date: Long, key: String, name:String, type: String, userName: String) {

        this.data = data
        this.date = date
        this.key = key
        this.name = name
        this.type = type
        this.username = userName
    }

    constructor(date: Long, name: String, size: Long, metadata:HashMap<String, String>, type: String, url: String, userName: String, key: String) {
        this.date =date
        this.name = name
        this.size = size
        this.type = type
        this.url = url
        this.metaData = metadata
        this.key = key

    }
}
