package capturesolution.capturesolutions.Objects

/**
 * Created by Gautam on 3/14/2018.
 */
class Report {
    var date: String? = null
    var dateKey: String? = null
    var key: String? = null
    var reportTitle: String? = null
    var reportType: String? = null
    var objectList: List<DataFile>? = null
    var createdById: String? = null
    var createdByName: String? = null
    var createdDate: Long? = null
    var createdByRole: String? = null

    constructor() {}

    constructor(date: String, dateKey: String, reportTitle: String, reportType:String, key:String) {
        this.date = date
        this.dateKey = dateKey
        this.key = key
        this.reportTitle = reportTitle
        this.reportType = reportType
    }

    constructor(date: String, dateKey: String, reportTitle: String, reportType:String, key:String, creatorId:String, creatorName:String, createdDate:Long, creatorRole:String) {
        this.date = date
        this.dateKey = dateKey
        this.key = key
        this.reportTitle = reportTitle
        this.reportType = reportType
        this.createdById = creatorId
        this.createdByName = creatorName
        this.createdDate = createdDate
        this.createdByRole = creatorRole
    }

    constructor(date: String, dateKey: String, reportTitle: String, reportType:String, key:String, Objects:List<DataFile>) {
        this.date = date
        this.dateKey = dateKey
        this.key = key
        this.reportTitle = reportTitle
        this.reportType = reportType
        this.objectList = Objects
    }

}