package capturesolution.capturesolutions.Objects

import android.net.Uri

/**
 * Created by Gautam on 3/12/2018.
 */
class Location {

    var address: String? = null
    var city: String? = null
    var key: String? = null
    var name: String? = null
    var state: String? = null
    var url: String? = null
    var zip: String? = null
    var localUri: Uri? = null

    constructor() {}


    constructor(add: String, city : String, key: String, name: String, state: String, url : String, zip: String) {
        this.address = add
        this.city = city
        this.key = key
        this.name = name
        this.state = state
        this.url = url
        this.zip = zip

    }

}