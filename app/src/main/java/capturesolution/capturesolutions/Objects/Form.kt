package capturesolution.capturesolutions.Objects

class Form {
    var data: String? =null
    var key: String? =null
    var name : String? = null

    constructor()

    constructor(data:String?, key:String?, name: String?){
        this.data = data
        this.key = key
        this.name = name
    }

}