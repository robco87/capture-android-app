package capturesolution.capturesolutions.Activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.support.v7.widget.AppCompatTextView
import android.text.InputType
import android.util.Log
import android.view.*

import org.json.JSONArray

import android.widget.*
import capturesolution.capturesolutions.Objects.DataFile
import capturesolution.capturesolutions.Objects.User

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.tab_location.*
import kotlinx.android.synthetic.main.tab_notes.*
import kotlinx.android.synthetic.main.tab_settings.*
import org.json.JSONObject
import java.util.*

import java.text.SimpleDateFormat

import android.graphics.drawable.ShapeDrawable

import android.graphics.Color
import android.graphics.drawable.shapes.RectShape
import android.support.v7.app.AlertDialog
import capturesolution.capturesolutions.R
import java.sql.Date


class FormFiller : AppCompatActivity() {
    var formData: String? =null
    var formKey: String? = null
    var formItemHolder: LinearLayout? = null
    var formLoading: ProgressBar? = null
    var viewIdList = mutableListOf<Int>()
    var viewJSONHash = hashMapOf<Int, JSONObject>()
    var checkHash = hashMapOf<String, List<Int>>()
    var radioHash = hashMapOf<String, List<Int>>()

    var anyEdits = false
    var formRef: String? = null
    var formattedRef:String? = null
    var user: User? = null

    var editMode = false

    var anyErrors = false
    var form: DataFile? =null

    var authKey: String? =null
    var locKey =""
    var date = ""
    var reportKey = ""
    var formTemplateName =""

    private var database: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_filler)
        centerTitle()
        database = FirebaseDatabase.getInstance()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Forms"


        formData = intent.getStringExtra("formData")
        editMode = intent.getBooleanExtra("isFormEdit",false)

        formTemplateName = intent.getStringExtra("formTemplateName")

        if(editMode){
            formKey = intent.getStringExtra("formKey")
            formRef = intent.getStringExtra("formItemPath")
            if(formRef!=null){
                formattedRef = formRef?.substring(database?.reference.toString().length)


                database?.getReference(formattedRef)?.addValueEventListener(object: ValueEventListener{
                    override fun onCancelled(p0: DatabaseError?) {
                    }

                    override fun onDataChange(p0: DataSnapshot?) {

                        form = p0?.getValue(DataFile::class.java)

                        //Log.d("database", form?.name)
                    }
                })
            }else{

            }


        }else{
            locKey = intent.getStringExtra("locKey")
            date = intent.getStringExtra("date")
            reportKey = intent.getStringExtra("reportKey")
        }
        getAccountKey()
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        formItemHolder = findViewById<LinearLayout>(R.id.formItemBox)
        formLoading = findViewById(R.id.formItemLoadingBar)

        val formArray = JSONArray(formData)

        val matchParentWidthLParam = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val wrapContentLParam = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        for (i in 0 until formArray.length()) {
            val formobject = formArray.getJSONObject(i)
            Log.d("form1", formobject.getString("type"))

            when(formobject.getString("type")){
                "date"->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.textSize = 17f
                    header.setPadding(0,15,0,0)
                    //header.setTypeface(header.typeface, Typeface.BOLD)

                    val dateView = EditText(this)

                    dateView.layoutParams = matchParentWidthLParam
                    dateView.textSize = 15f
                    dateView.maxLines = 1
                    dateView.inputType = InputType.TYPE_NULL


                    dateView.id = View.generateViewId()
                    viewIdList.add(dateView.id)
                    viewJSONHash.put(dateView.id,formobject)
                    var date: String? =null
                    var defCal = Calendar.getInstance()
                    var selYear = defCal.get(Calendar.YEAR)
                    var selMonth = defCal.get(Calendar.MONTH)
                    var day = defCal.get(Calendar.DAY_OF_MONTH)
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    if(formobject.has("value")){
                        date = formobject.getString("value")
                        dateView.setText(date)

                        val time = sdf.parse(date)
                        val dateCal = Calendar.getInstance()
                        dateCal.time = time
                        selYear = dateCal.get(Calendar.YEAR)
                        selMonth = dateCal.get(Calendar.MONTH)
                        day = dateCal.get(Calendar.DAY_OF_MONTH)
                    }
                    var datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                                val selDate = Date(year-1900, month, dayOfMonth)
                                Log.d("SelDate",selDate.toString())

                                var dateString = sdf.format(selDate)
                                dateView.setText(dateString.toString())

                            }, selYear,selMonth,day)
                    dateView.setOnFocusChangeListener { v, hasFocus ->
                        if(hasFocus){
                            anyEdits = true
                            datePickerDialog.show()
                        }
                    }

                    dateView.setOnClickListener {
                        anyEdits = true
                        datePickerDialog.show()
                    }

                    //need to bring up date selector
                    formItemHolder?.addView(header)
                    formItemHolder?.addView(dateView)
                }
                "checkbox-group"->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.setPadding(0,15,0,0)
                    header.textSize = 17f

                    var valArray = formobject.getJSONArray("values")
                    formItemHolder?.addView(header)

                    for(m in 0 until valArray.length()){
                        var checkboxDetails = valArray[m] as JSONObject
                        var checkBox = CheckBox(this)

                        checkBox.id = View.generateViewId()
                        viewIdList.add(checkBox.id)
                        viewJSONHash.put(checkBox.id,formobject)

                        checkBox.text = checkboxDetails.getString("label")

                        if(checkHash.containsKey(formobject.getString("name"))){//checkhash has list of checkbox ids associated with group
                            var checkboxList = checkHash.get(formobject.getString("name")) as MutableList //get view ids for existing group
                            checkboxList.add(checkBox.id)//add new checkbox view id to list
                            checkHash.put(formobject.getString("name"),checkboxList)//save new list to hash
                        }else{
                            var checkboxList = mutableListOf<Int>()//new checkbox id list
                            checkboxList.add(checkBox.id)//add current checkbox id to id list
                            checkHash.put(formobject.getString("name"),checkboxList)//save id list to checkbox group
                        }

                        if(checkboxDetails.has("selected")){
                            checkBox.isChecked = true
                        }

                        checkBox.setOnCheckedChangeListener { compoundButton, b ->
                            anyEdits = true
                        }

                        formItemHolder?.addView(checkBox)
                    }

                }
                "header"->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.textSize = 24f
                    header.setPadding(0,15,0,0)
                    header.setTypeface(header.typeface, Typeface.BOLD)
                    header.id = View.generateViewId()
                    viewIdList.add(header.id)
                    viewJSONHash.put(header.id,formobject)

                    formItemHolder?.addView(header)
                }
                "number" ->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.textSize = 17f
                    header.setPadding(0,15,0,0)
                    //header.setTypeface(header.typeface, Typeface.BOLD)

                    val numView = EditText(this)
                    numView.layoutParams = matchParentWidthLParam
                    numView.textSize = 15f
                    numView.maxLines = 1
                    numView.inputType = InputType.TYPE_CLASS_NUMBER

                    numView.id = View.generateViewId()
                    viewIdList.add(numView.id)
                    viewJSONHash.put(numView.id,formobject)

                    if(formobject.has("value")){
                        numView.setText(formobject.getString("value"))
                    }
                    if(formobject.has("placeholder")){
                        numView.hint = formobject.getString("placeholder")
                    }
                    numView.setOnClickListener {
                        anyEdits = true
                    }


                    formItemHolder?.addView(header)
                    formItemHolder?.addView(numView)
                }
                "paragraph" ->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.setPadding(0,15,0,0)
                    header.text = formobject.getString("label")
                    header.textSize = 17f

                    header.id = View.generateViewId()
                    viewIdList.add(header.id)
                    viewJSONHash.put(header.id,formobject)

                    formItemHolder?.addView(header)
                }
                "radio-group"->{

                    val radioGroup = RadioGroup(this)
                    radioGroup.orientation = RadioGroup.VERTICAL

                    radioGroup.id = View.generateViewId()
                    radioGroup.tag = "radioGroup"
                    viewIdList.add(radioGroup.id)
                    viewJSONHash.put(radioGroup.id,formobject)


                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.setPadding(0,15,0,0)
                    header.textSize = 17f

                    var valArray = formobject.getJSONArray("values")

                    for(j in 0 until valArray.length()){
                        var radioButton = RadioButton(this)
                        var radioDetails = valArray[j] as JSONObject
                        radioButton.text = radioDetails.getString("label")
                        radioButton.id = View.generateViewId()

                        if(radioDetails.has("selected")){
                            radioButton.isChecked = true
                        }

                        if(radioHash.containsKey(formobject.getString("name"))){//checkhash has list of checkbox ids associated with group
                            var radioButtonList = radioHash.get(formobject.getString("name")) as MutableList //get view ids for existing group
                            radioButtonList.add(radioButton.id)//add new checkbox view id to list
                            radioHash.put(formobject.getString("name"),radioButtonList)//save new list to hash
                        }else{
                            var radioButtonList = mutableListOf<Int>()//new checkbox id list
                            radioButtonList.add(radioButton.id)//add current checkbox id to id list
                            radioHash.put(formobject.getString("name"),radioButtonList)//save id list to checkbox group
                        }

                        radioButton.setOnCheckedChangeListener { compoundButton, b ->
                            anyEdits = true
                        }

                        radioGroup.addView(radioButton)
                    }

                    formItemHolder?.addView(header)
                    formItemHolder?.addView(radioGroup)

                }
                "select" ->{
                    var list  = mutableListOf<String>()

                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.setPadding(0,15,0,0)
                    header.textSize = 17f

                    val spinner = Spinner(this)
                    spinner.layoutParams = matchParentWidthLParam
                    spinner.id = View.generateViewId()
                    viewIdList.add(spinner.id)
                    viewJSONHash.put(spinner.id,formobject)


                    var selected :String? = null

                    val valArray = formobject.getJSONArray("values")

                    for(k in 0 until valArray.length()){
                        var listItem = valArray[k] as JSONObject
                        list.add(listItem.getString("label"))

                        if(listItem.has("selected")){
                            selected = listItem.getString("label")
                        }
                    }

                    var aa = ArrayAdapter(this@FormFiller, android.R.layout.simple_spinner_item, list)
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner.adapter = aa
                    spinner.setSelection(aa.getPosition(selected))
                    Log.d("aa", aa.toString())

                    if(formobject.has("placeholder")){
                        list.add(formobject.getString("placeholder"))
                        if(selected==null || !editMode){
                            spinner.setSelection(aa.getPosition(formobject.getString("placeholder")))
                        }
                    }else{

                    }
                    formItemHolder?.addView(header)
                    formItemHolder?.addView(spinner)

                }
                "text" ->{
                    val header = TextView(this)
                    header.layoutParams = wrapContentLParam
                    header.text = formobject.getString("label")
                    header.setPadding(0,15,0,0)
                    header.textSize = 17f

                    val enterText = EditText(this)
                    enterText.layoutParams = matchParentWidthLParam
                    enterText.textSize = 15f
                    enterText.maxLines = 1
                    enterText.inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

                    enterText.id = View.generateViewId()
                    viewIdList.add(enterText.id)
                    viewJSONHash.put(enterText.id,formobject)

                    //need to save edited text
                    if(formobject.has("value")){
                        enterText.setText(formobject.getString("value"))
                    }
                    if(formobject.has("placeholder")){
                        enterText.hint = formobject.getString("placeholder")
                    }
                    enterText.setOnClickListener {
                        anyEdits = true
                    }

                    formItemHolder?.addView(header)
                    formItemHolder?.addView(enterText)

                }
                "textarea" ->{
                    val header = TextView(this)
                    header.layoutParams = matchParentWidthLParam
                    header.text = formobject.getString("label")
                    header.setPadding(0,15,0,0)
                    header.textSize = 17f

                    val enterText = EditText(this)
                    enterText.layoutParams = matchParentWidthLParam
                    enterText.textSize = 15f
                    enterText.isVerticalScrollBarEnabled = true
                    enterText.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    enterText.minHeight = 30

                    enterText.id = View.generateViewId()
                    viewIdList.add(enterText.id)
                    viewJSONHash.put(enterText.id,formobject)

                    if(formobject.has("value")){
                        enterText.setText(formobject.getString("value"))
                    }
                    if(formobject.has("placeholder")){
                        enterText.hint = formobject.getString("placeholder")
                    }
                    //need to save and load edited text
                    enterText.setOnClickListener {
                        anyEdits = true
                    }
                    formItemHolder?.addView(header)
                    formItemHolder?.addView(enterText)
                }

            }
            val image = ImageView(this)
            image.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 10)
            val badge = ShapeDrawable(RectShape())
            badge.intrinsicWidth = ViewGroup.LayoutParams.MATCH_PARENT
            badge.intrinsicHeight = ViewGroup.LayoutParams.MATCH_PARENT
            badge.paint.color = Color.GRAY
            image.setImageDrawable(badge)
            formItemHolder?.addView(image)

            formLoading?.visibility = View.INVISIBLE

        }

        locationTabView.setOnClickListener {
            startActivity(Intent(this@FormFiller, MainActivity::class.java))
        }
        val intent = Intent(this@FormFiller, MainActivity::class.java)
        notesTabView.setOnClickListener {
            intent.putExtra("tabSel",1)
            startActivity(intent)
        }
        settingsTabView.setOnClickListener {
            intent.putExtra("tabSel",2)
            startActivity(intent)
        }

    }



    fun saveForm(){
        var saveJSONArray = JSONArray()
        var firstCheck = 0

        for(viewId in viewIdList.iterator()){
            var formObject = viewJSONHash.get(viewId) as JSONObject//formobject
            var view = findViewById<View>(viewId)
            Log.d("ViewID1",viewId.toString())
            when(formObject.getString("type")) {
                "date"->{
                    view as EditText
                    if(view.text.isNotBlank()) {
                        formObject.put("value", view.text)
                    }
                    saveJSONArray.put(formObject)
                }
                "checkbox-group"->{
                    view as CheckBox
                    var groupCheckBoxList = checkHash.get(formObject.getString("name")) as MutableList//get the checkbox view ids associated with the group
                    if(view.isChecked){
                        //groupCheckBoxList.indexOf(viewId) -- this gives the index of the checkbox view
                        formObject.getJSONArray("values").getJSONObject(groupCheckBoxList.indexOf(viewId)).put("selected",true)//use the index of the checkbox view to set the correct item within json array to selected value
                    }
                    if(!view.isChecked){
                        formObject.getJSONArray("values").getJSONObject(groupCheckBoxList.indexOf(viewId)).remove("selected")//use the index of the checkbox view to set the correct item within json array to selected value

                    }
                    if(firstCheck == 0){
                        firstCheck = viewId
                    }else{

                    }
                    //Log.d("FirstCheck",firstCheck.toString() + groupCheckBoxList.size + viewId)
                    if(firstCheck+groupCheckBoxList.size==viewId+1) {
                        saveJSONArray.put(formObject)
                        firstCheck = 0
                    }


                }
                "header"->{
                    view as TextView
                    saveJSONArray.put(formObject)
                }
                "paragraph"->{
                    view as TextView
                    saveJSONArray.put(formObject)
                }
                "number"->{
                    view as EditText
                    if(view.text.isNotBlank()) {
                        formObject.put("value", view.text)
                    }
                    saveJSONArray.put(formObject)
                }
                "radio-group"->{
                    view as RadioGroup
                    val selRadioButtonId = view.checkedRadioButtonId

                    val radioButtonViewIdList = radioHash.get(formObject.getString("name")) as List<Int>

                    radioButtonViewIdList.forEach {
                        Log.d("RadioList", radioButtonViewIdList.toString() + selRadioButtonId + it)
                        formObject.getJSONArray("values").getJSONObject(radioButtonViewIdList.indexOf(it)).remove("selected")
                        if(it==selRadioButtonId){
                            formObject.getJSONArray("values").getJSONObject(radioButtonViewIdList.indexOf(selRadioButtonId)).put("selected", true)
                        }
                    }
                    saveJSONArray.put(formObject)
                }
                "select" ->{
                    view as Spinner
                    //Log.d("selected item",view.selectedItem.toString())
                    val valArray = formObject.getJSONArray("values")
                    var list  = mutableListOf<String>()

                    for(k in 0 until valArray.length()){
                        var listItem = valArray[k] as JSONObject
                        list.add(listItem.getString("label"))
                    }

                    if(formObject.has("placeholder")){
                        if(view.selectedItem.toString()!=formObject.getString("placeholder")){
                            list.forEach  {
                                if(it!=formObject.getString("placeholder")){
                                    formObject.getJSONArray("values").getJSONObject(list.indexOf(it)).remove("selected")
                                    if(it==view.selectedItem.toString()){
                                        formObject.getJSONArray("values").getJSONObject(list.indexOf(it)).put("selected",1)
                                    }
                                }

                            }

                        }else{
                            //anyErrors = true
                            //Toast.makeText(this,"Please Select an Option for "+ formObject.getString("label"),Toast.LENGTH_LONG).show()
                        }
                    }else{

                        list.forEach  {
                            formObject.getJSONArray("values").getJSONObject(list.indexOf(it)).remove("selected")
                            if(it==view.selectedItem.toString()){
                                formObject.getJSONArray("values").getJSONObject(list.indexOf(it)).put("selected",1)
                                }
                        }
                    }

                    saveJSONArray.put(formObject)
                }
                "text"->{
                    view as EditText
                    if(view.text.isNotBlank()) {
                        formObject.put("value", view.text)
                    }
                    saveJSONArray.put(formObject)
                }
                "textarea"->{
                    view as EditText
                    if(view.text.isNotBlank()) {
                        formObject.put("value", view.text)
                    }
                    saveJSONArray.put(formObject)
                }
            }
            //Log.d("JSON",saveJSONArray.toString())
        }
        if(!anyErrors){
            formLoading?.visibility = View.VISIBLE
            if(editMode && form!=null){

                form?.data = saveJSONArray.toString()
                form?.date = Calendar.getInstance().timeInMillis/1000
                form?.username = user?.Name

                var updateFormIntent = Intent(this@FormFiller, NewReport::class.java)

                //Log.d("formfile", dataFile.data)

                updateFormIntent.putExtra("datafile",form)

                updateFormIntent.putExtra("authKey",authKey)
                updateFormIntent.putExtra("key",locKey)
                //newReport.putExtra("date",date)
                updateFormIntent.putExtra("reportKey",reportKey)
                updateFormIntent.putExtra("userRole",user?.Role)

                //newForm.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                setResult(Activity.RESULT_OK, updateFormIntent)
                finish()
                //startActivity(newForm)
                //
            }
            else{
                var newForm = Intent(this@FormFiller, NewReport::class.java)
                var formSaveKey: String ? = null
                //Log.d("formfile", formKey)
                if(formKey == null){
                    formSaveKey= database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(date)?.child(reportKey)?.child("Objects")?.push()?.key + Random().nextInt(10000)//key for new report
                    Log.d("formfile", formSaveKey)
                }else{
                    formSaveKey = formKey
                }

                var dataFile = DataFile(saveJSONArray.toString(), Calendar.getInstance().timeInMillis/1000,formSaveKey!!,formTemplateName,"Form",user?.Name.toString())




                newForm.putExtra("datafile",dataFile)

                newForm.putExtra("authKey",authKey)
                newForm.putExtra("key",locKey)
                //newReport.putExtra("date",date)
                newForm.putExtra("reportKey",reportKey)
                newForm.putExtra("userRole",user?.Role)

                //newForm.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                setResult(Activity.RESULT_OK, newForm)
                finish()
                //startActivity(newForm)
                //

            }
        }

    }


    private fun centerTitle() {
        val textViews = ArrayList<View>()
        window.decorView.findViewsWithText(textViews, title, View.FIND_VIEWS_WITH_TEXT)
        if (textViews.size > 0) {
            var appCompatTextView: AppCompatTextView? = null
            if (textViews.size == 1)
                appCompatTextView = textViews[0] as AppCompatTextView
            else {
                for (v in textViews) {
                    if (v.parent is Toolbar) {
                        appCompatTextView = v as AppCompatTextView
                        break
                    }
                }
            }
            if (appCompatTextView != null) {
                val params = appCompatTextView.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                appCompatTextView.layoutParams = params
                appCompatTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
                //appCompatTextView.setPadding(-100,5,0,5)
            }
        }
    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId!!.equals(android.R.id.home)) {
            if(anyEdits){
                //add popup to confirm
                loadSaveDialog()

            }else{
                finish()
            }
            return true
        }
        if(item.itemId.equals(R.id.save_form)){
            saveForm()
            //onBackPressed()
            return true
        }
        else{
            return true
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(anyEdits){
            //add popup to confirm
            loadSaveDialog()

        }else{
            finish()
        }

    }

    fun loadSaveDialog(){
        val builder = AlertDialog.Builder(this@FormFiller)
        // Get the layout inflater
        builder.setTitle("Save Form")
        builder.setMessage("Did you want to save your form?")
        builder.setPositiveButton("Save", object: DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                saveForm()
            }
        })
        builder.setNegativeButton("No", object: DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                finish()
            }
        })
        builder.create().show()

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_save_form, menu)
        return true
    }

    fun getAccountKey() {
        val reference = database?.getReference("Users")
        val loggedUser = FirebaseAuth.getInstance().currentUser
        val queryRef = reference
        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {

                for (ds in p0!!.children) {
                    var duser = ds?.getValue(User::class.java)
                    duser?.id = ds.key

                    if (duser?.id.equals(loggedUser?.uid)) {
                        if(duser?.Role!="deleted") {
                            authKey = duser?.Key
                            user = duser
                        }
                        else{

                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot?) {

            }
        })
    }
}
