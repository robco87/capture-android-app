package capturesolution.capturesolutions.Activities


import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import capturesolution.capturesolutions.Objects.DataFile
import capturesolution.capturesolutions.Objects.Location
import capturesolution.capturesolutions.Objects.Report
import capturesolution.capturesolutions.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import de.cketti.mailto.EmailIntentBuilder
import kotlinx.android.synthetic.main.activity_date_details.*
import kotlinx.android.synthetic.main.content_report_details.*
import kotlinx.android.synthetic.main.tab_location.*
import kotlinx.android.synthetic.main.tab_notes.*
import kotlinx.android.synthetic.main.tab_settings.*
import java.text.SimpleDateFormat


class DateDetails : AppCompatActivity(), NewReport.EditReportData {
    private var database: FirebaseDatabase? = null
    private var auth: FirebaseAuth? = null
    var locKey: String? = null
    var authKey: String? = null
    private var userRole: String? = null
    var loc: Location? = null
    var date: String? = null
    var locImg: ImageView? = null
    var reportList: MutableList<Report>? = mutableListOf()
    var selReport: Report? = null
    var reportAdapter: ReportListAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        var data = intent.extras
        locKey = data.getString("key")
        authKey = data.getString("authKey")
        date = data.getString("date")
        userRole = data.getString("userRole")
        locImg = findViewById(R.id.locImg)

        if(userRole=="admin") {
            reportListview.setOnItemLongClickListener { adapterView, view, i, l ->
                val dialog = AlertDialog.Builder(this@DateDetails)
                dialog.setTitle("Delete Report?")
                dialog.setPositiveButton("Delete", object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        val delReport = reportAdapter?.getReportItem(i)
                        database?.reference?.child("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(delReport?.dateKey)?.child(delReport?.key)?.removeValue()

                    }
                })
                dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {

                    }
                })
                dialog.create().show()

                true
            }
        }


        locationTabView.setOnClickListener {
            startActivity(Intent(this@DateDetails, MainActivity::class.java))
        }
        val intent = Intent(this@DateDetails, MainActivity::class.java)
        notesTabView.setOnClickListener {
            intent.putExtra("tabSel",1)
            startActivity(intent)
        }
        settingsTabView.setOnClickListener {
            intent.putExtra("tabSel",2)
            startActivity(intent)
        }

        reportListview.setOnItemClickListener { adapterView, view, i, l ->
            selReport = reportAdapter?.getReportItem(i)
            progressBar2.visibility = View.VISIBLE
            val intent = Intent(this@DateDetails, NewReport::class.java)
            intent.putExtra("key",locKey)
            intent.putExtra("authKey",authKey)
            intent.putExtra("reportKey", selReport?.key)
            intent.putExtra("date", date)
            intent.putExtra("reportType",selReport?.reportType)
            intent.putExtra("userRole",userRole)
            startActivity(intent)
        }

        loadDetails()
    }

    override fun onResume() {
        super.onResume()
        progressBar2.visibility = View.INVISIBLE
    }

    private fun loadDetails(){

        val reference = database?.getReference("Locations")
        val queryLoc = reference?.child(authKey)

        val reportReference = database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")
        val currDate = SimpleDateFormat("MMM dd, yyyy").parse(date)
        val dbDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val newDateFormat = SimpleDateFormat("MMMM dd, yyyy")

        val queryReport = reportReference?.child(dbDateFormat.format(currDate).toString())

        queryLoc?.addChildEventListener(object: ChildEventListener {

            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                if(p0?.key.equals(locKey)){
                    loc = p0?.getValue(Location::class.java)
                    if(loc?.url == null){
                        loc?.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                    }else{

                    }
                    try {
                        Glide.with(locImg?.context).load(loc?.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object : RequestListener<Drawable> {
                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                progressBar3.visibility = View.INVISIBLE
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return true
                            }
                        }).into(locImg)
                    }catch (e: IllegalArgumentException){

                    }


                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                    dateText?.text= newDateFormat.format(currDate).toString()
                }
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                if(p0?.key.equals(locKey)){
                    loc = p0?.getValue(Location::class.java)
                    if(loc?.url == null){
                        loc?.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                    }else{

                    }
                    try {
                        Glide.with(locImg?.context).load(loc?.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object : RequestListener<Drawable> {
                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                progressBar3.visibility = View.INVISIBLE
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return true
                            }
                        }).into(locImg)
                    }catch (e: IllegalArgumentException){

                    }

                    /*Picasso.get().load(loc?.url).resize(225, 225).placeholder(R.drawable.ic_launcher_background).into(locImg, object: Callback {
                        override fun onSuccess() {
                            progressBar3.visibility = View.INVISIBLE
                        }

                        override fun onError(e: Exception?) {

                        }
                    })*/


                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                    dateText?.text= newDateFormat.format(currDate).toString()
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
            }
        })
        queryReport?.addChildEventListener(object: ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                var newReport = p0?.getValue(Report::class.java)//new object

                var oldReport = reportList?.find { it.key == newReport?.key }//finding old object
                reportList?.remove(oldReport)//removing old obj

                val reportObjectList: MutableList<DataFile> = mutableListOf()
                for (ds in p0?.child("Objects")?.children?.iterator()!!) {
                    val reportObj = ds.getValue(DataFile::class.java)//add
                    reportObjectList.add(reportObj!!)//adding report to obj list
                    newReport?.objectList = reportObjectList
                }
                reportList?.add(newReport!!)//add new obj

                if(userRole == "basic"){
                    reportList?.retainAll{ it.createdByRole =="basic"}
                }
                if(userRole == "manager"){
                    reportList?.removeAll{it.createdByRole == "admin"}
                }
                if(userRole == "admin"){

                }

                reportAdapter?.notifyDataSetChanged()//refresh adapter
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                var report = p0?.getValue(Report::class.java)

                if (!reportList!!.contains(report)) {

                    val reportObjectList: MutableList<DataFile> = mutableListOf()
                    for (ds in p0?.child("Objects")?.children?.iterator()!!) {
                        val reportObj = ds.getValue(DataFile::class.java)//add
                        reportObjectList.add(reportObj!!)//adding report to obj list
                        report?.objectList = reportObjectList
                    }
                    //Log.d("Datashot1", reportList?.size.toString())
                    //report = getLocObjects(report!!)
                    reportList?.add(report!!)
                    //Log.d("Report Data", reportList?.toString())

                } else {

                }

                //Log.d("filteredList", filteredList!!.toMutableList().toString())
                if (reportAdapter == null) {

                    if(userRole == "basic"){
                        reportList?.retainAll{ it.createdByRole =="basic" && it.createdById == auth?.currentUser?.uid}
                    }
                    if(userRole == "manager"){
                        reportList?.removeAll{it.createdByRole == "admin"}
                        if(report?.createdByRole == "manager" && report.createdById != auth?.currentUser?.uid){
                            reportList?.remove(report)
                        }
                    }
                    if(userRole == "admin"){

                    }
                    reportAdapter = ReportListAdapter(this@DateDetails, reportList!!, loc!!, date!!, userRole!!)
                    reportListview?.adapter = reportAdapter
                }
                else {
                    if (userRole == "basic") {
                        reportList?.retainAll { it.createdByRole == "basic" && it.createdById == auth?.currentUser?.uid }
                    }
                    if (userRole == "manager") {
                        reportList?.removeAll { it.createdByRole == "admin" }
                        if (report?.createdByRole == "manager" && report.createdById != auth?.currentUser?.uid) {
                            reportList?.remove(report)
                        }
                    }
                    if(userRole == "admin"){

                    }
                }
                reportAdapter?.notifyDataSetChanged()
            }

                //adapter here

            override fun onChildRemoved(p0: DataSnapshot?) {
                val delReport = p0?.getValue(Report::class.java)
                reportList?.remove(reportList?.find { it.key == delReport?.key })
                reportAdapter?.notifyDataSetChanged()
            }
        })

    }

    class ReportListAdapter(private var activity: DateDetails, var reports: MutableList<Report>, var location: Location, var date: String, var userRole: String):BaseAdapter(){

        private class ReportViewHolder(row:View?){
            var repName: TextView? = null
            var repType: TextView? = null
            var share: ImageButton? = null
            var creator: TextView? = null
            var repObjTypeImgLayout: LinearLayout? = null

            init {
                this.repName = row?.findViewById(R.id.reportName)
                this.repType = row?.findViewById(R.id.newReportType)
                this.repObjTypeImgLayout = row?.findViewById(R.id.imageLayout)
                this.share = row?.findViewById(R.id.shareButton)
                this.creator = row?.findViewById(R.id.creator)
            }
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view: View
            val report = getItem(p0) as Report
            val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.report_details_row, null)
            val viewHolder = ReportViewHolder(view)
            view?.tag = viewHolder
            if(report.createdDate !=null) {
                var date = report.createdDate!! * 1000
                var format = SimpleDateFormat("h:mm a MMMM d, yyyy")
                viewHolder.creator?.text = report.createdByName + " " + format.format(date)
            }

            viewHolder.repType?.text = report.reportType
            viewHolder.repName?.text = report.reportTitle


            if(report.objectList!=null){
            for (reportObject in report.objectList!!.iterator()) {
                var img = ImageView(activity.baseContext)
                img.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                img.setPadding(5,2,0,3)
                when (reportObject.type) {
                    "Photo" -> {
                        img.setImageResource(R.drawable.ic_add_a_photo_white_24dp)
                    }
                    "Note" -> {
                        img.setImageResource(R.drawable.ic_list_white_24dp)
                    }
                    "Video" ->{
                        img.setImageResource(R.drawable.ic_video_call_white_24dp)
                    }
                    "Audio" ->{
                        img.setImageResource(R.drawable.ic_volume_up_white_24dp)
                    }
                    "Form" ->{
                        img.setImageResource(R.drawable.ic_assignment_accent_24dp)
                    }
                }
                viewHolder.repObjTypeImgLayout?.addView(img)
            }
            }

            viewHolder.share?.visibility = View.VISIBLE
            viewHolder.share?.setOnClickListener {
                createUrlString(report)
                createEmail(report)
            }
            return view
        }

        override fun getItem(p0: Int): Any {
            val sortedDates = reports.sortedBy { it.createdDate }.asReversed()
            val sortedDatesRO : List<Report> = sortedDates
            return sortedDatesRO[p0]
        }

        override fun getCount(): Int {
            return  reports.size
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        fun getReportItem(p0: Int): Report{
            val sortedDates = reports.sortedBy { it.createdDate }.asReversed()
            val sortedDatesRO : List<Report> = sortedDates
            return sortedDatesRO[p0]
        }

        fun createEmail(report: Report){
            var sendReport = EmailIntentBuilder.from(activity)
            sendReport.subject("Report Export: " + location.name + ", " + date)
            sendReport.body("Report Export \n" +
                    location.name + "\n" +
                    date + "\n" +
                    report.reportType + "\n" +
                    report.reportTitle + "\n" + "\n"+
                    "Report Items: "+ "\n" + createUrlString(report)
            ).start()
        }

        fun createUrlString(report: Report?):String{
            var photoLinks: String? = " "
            if(report?.objectList!=null) {
                for (df in report.objectList!!.iterator()) {
                    if(df.type != "Note") {
                        photoLinks = photoLinks + df.type + ": \n"
                        photoLinks = photoLinks + (df.url) + ": \n"
                        Log.d("reportType", photoLinks)
                    }else {
                        photoLinks = photoLinks + df.type + ":\n"
                        photoLinks = photoLinks + "Title: " + df.title + "\n"
                        photoLinks = photoLinks + "Body: " + df.note + "\n"
                    }
                }
            }
            return photoLinks!!
        }

    }

    override fun returnReport(): Report? {
        return selReport
    }

    override fun returnLocObjects(): List<DataFile>? {
        return selReport?.objectList
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId!!.equals(android.R.id.home)){
            finish()
        }
        if(item.itemId.equals(R.id.add_new)){
            val intent = Intent(this@DateDetails, NewReport::class.java)
            intent.putExtra("key",locKey)
            intent.putExtra("authKey",authKey)
            intent.putExtra("date", date)
            intent.putExtra("userRole", userRole)
            startActivity(intent)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        var format = SimpleDateFormat("MMM dd, yyyy")
        var a = format.format(System.currentTimeMillis())

        inflater.inflate(R.menu.menu_main, menu)

        return true
    }


}
