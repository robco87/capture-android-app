package capturesolution.capturesolutions.Activities


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity

import android.view.WindowManager
import android.widget.ListView

import capturesolution.capturesolutions.Fragments.LocationListFragment
import capturesolution.capturesolutions.Fragments.NotesFragment
import capturesolution.capturesolutions.Fragments.Settings
import capturesolution.capturesolutions.Objects.Location

import capturesolution.capturesolutions.R

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var locList: ListView? = null
    //var locListAdapter: LocListAdapter? = null
    var database: FirebaseDatabase? = null

    private var mAuth: FirebaseAuth? = null
    private var locListObject: MutableList<Location>? = mutableListOf<Location>()
    private var authKey: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        setTitle("")
        supportActionBar?.hide()

        var user = mAuth?.currentUser
        if (user == null) {
            val intent = Intent(this, Selector::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }else{

        }
        //window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)


        //Picasso.get().load(R.drawable.capture_logo_header).resize(600,150).centerInside().into(titleImage)

        val vpPager = findViewById(R.id.viewpager) as ViewPager
        var adapterViewPager = PagerAdapter(getSupportFragmentManager())
        vpPager.adapter = adapterViewPager


        if (intent != null) {
            var a = intent.getIntExtra("tabSel", 0)
            viewpager.currentItem = a

        }


        pager_header.setupWithViewPager(vpPager)
        pager_header.getTabAt(0)?.setCustomView(R.layout.tab_location)
        pager_header.getTabAt(1)?.setCustomView(R.layout.tab_notes)
        pager_header.getTabAt(2)?.setCustomView(R.layout.tab_settings)

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 1) {

                    //supportActionBar?.setCustomView(R.layout.toolbar_prefs)
                }
                if (position == 2) {


                }
                if(position ==0){

                    /*toolbar_title.visibility = View.VISIBLE
                    supportActionBar?.setDisplayUseLogoEnabled(true)
                    supportActionBar?.title = ""*/
                }
            }
        })

    }


    class PagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        private val NUM_ITEMS = 3

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 // Fragment # 0 - This will show FirstFragment
                -> return LocationListFragment.newInstance("", "")

                1
                -> return NotesFragment.newInstance("", "")

                2 // Fragment # 0 - This will show FirstFragment different title
                -> return Settings.newInstance("", "") as Fragment

                else -> return LocationListFragment.newInstance("", "")
            }
        }

        override fun getCount(): Int {
            return NUM_ITEMS
        }



        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0) {
                return "Reports"
            }
            if (position == 1) {
                return "Notes"
            }
            if (position == 2) {
                return "Settings"
            } else {
                return "default"
            }
        }


    }


    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack("aa")
        fragmentTransaction.commit()
    }

    fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }

    fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { replace(frameId, fragment) }
    }

    override fun onBackPressed() {
        this.finish()
    }
}
