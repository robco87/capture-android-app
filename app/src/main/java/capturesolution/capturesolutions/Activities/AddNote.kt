package capturesolution.capturesolutions.Activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.EditText
import capturesolution.capturesolutions.R

import kotlinx.android.synthetic.main.activity_add_note.*
import android.widget.ImageView
import capturesolution.capturesolutions.Objects.DataFile
import capturesolution.capturesolutions.Objects.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import java.util.*


class AddNote : AppCompatActivity() {

    var noteTitleEditText: EditText? =  null
    var noteContentEditText: EditText? =  null
    private var mAuth: FirebaseAuth? = null
    var database: FirebaseDatabase? = null
    private var authKey: String? = null
    var user: User? = null
    var key: String? = null
    var editNoteItem = false
    var dbRef:String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        supportActionBar?.title = ""
        setSupportActionBar(toolbar)

        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        noteContentEditText = findViewById(R.id.noteContent)
        noteTitleEditText = findViewById(R.id.noteTitle)

        getAccountKey()

        if(intent.hasExtra("editMode")){
            key = intent.getStringExtra("editMode")
            noteTitleEditText?.setText(intent.getStringExtra("editNoteTitle"))
            noteContentEditText?.setText(intent.getStringExtra("editNoteContent"))
        }

        if(intent.hasExtra("editNoteItemPath")){
            editNoteItem = true
            key = intent.getStringExtra("editNoteKey")
            dbRef = intent.getStringExtra("editNoteItemPath")
            noteTitleEditText?.setText(intent.getStringExtra("editNoteTitle"))
            noteContentEditText?.setText(intent.getStringExtra("editNoteContent"))
        }

        val titleImage = findViewById<ImageView>(R.id.toolbar_title)

        Picasso.get().load(R.drawable.capture_logo_header).fit().centerInside().into(titleImage)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater?.inflate(R.menu.menu_add_note, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                finish()
                return true
            }

            R.id.save ->{
                val noteTitle = noteTitleEditText?.text.toString()
                val noteContent = noteContentEditText?.text.toString()
                var createdNote = DataFile(Calendar.getInstance().timeInMillis/1000,noteContent,noteTitle,"Note",user?.Name!!)

                if(key == null) {
                    key = database?.getReference("Notes")?.child(authKey)?.push()?.key
                }
                if(editNoteItem){
                    var a = dbRef?.substring(database?.reference.toString().length)
                    createdNote.key = key
                    database?.getReference(a)?.setValue(createdNote)
                }else{
                    createdNote.key = key
                    database?.getReference("Notes")?.child(authKey)?.child(user?.id)?.child(key)?.setValue(createdNote)
                }

                finish()

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getAccountKey() {
        val reference = database?.getReference("Users")
        val loggedUser = FirebaseAuth.getInstance().currentUser
        val queryRef = reference
        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {

                for (ds in p0!!.children) {
                    var duser = ds?.getValue(User::class.java)
                    duser?.id = ds.key

                    if (duser?.id.equals(loggedUser?.uid)) {
                        if(duser?.Role!="deleted") {
                            authKey = duser?.Key
                            user = duser
                        }
                        else{

                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot?) {

            }
        })
    }









}
