package capturesolution.capturesolutions.Activities

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.ScaleAnimation
import android.widget.*
import capturesolution.capturesolutions.Objects.*
import capturesolution.capturesolutions.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.tasks.OnCompleteListener

import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask


import kotlinx.android.synthetic.main.activity_new_report.*
import kotlinx.android.synthetic.main.content_new_report.*
import kotlinx.android.synthetic.main.tab_location.*
import kotlinx.android.synthetic.main.tab_notes.*
import kotlinx.android.synthetic.main.tab_settings.*

import java.io.File
import java.io.InputStream

import java.text.SimpleDateFormat
import java.util.*


class NewReport : AppCompatActivity() {

    private var authKey: String? = null
    private var locKey: String? = null
    private var reportKey: String? = null
    private var editMode: Boolean = false
    var existinReport: Report? = null
    private var pastDateAddReport: Boolean? = false
    //var mCurrentPhotoPath: String? = null
    var currentAudioPath: String? = null
    var currentAudioName: String? = null

    var aa : ArrayAdapter<String>? = null

    var photoDataFile: DataFile? = null
    var videoDataFile: DataFile? = null

    private var reportTypeList: MutableList<String>? = mutableListOf<String>()


    var locImg: ImageView? = null
    var address: TextView? = null
    var name: TextView? = null
    var progBar: ProgressBar? = null
    var currDate: TextView? = null
    var repTitle: EditText? = null
    var reportItemList: ListView? = null

    var date: String? = null
    var dateDB: String? = null
    var reportTypeSpinner: Spinner? = null
    var takePic: ImageButton? = null
    var takeAudio: ImageButton? = null
    var takeNote: ImageButton? = null
    var takeForm: ImageButton? = null
    var takeFromGallery: ImageButton? = null
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_VIDEO_CAPTURE = 2
    val IMAGE_PICK_CODE = 3110
    var photoURI: Uri? = null

    var photoURIlist: MutableList<Uri>? = mutableListOf<Uri>()
    var reportItemDataFileList: MutableList<DataFile> = mutableListOf<DataFile>()

    private var database: FirebaseDatabase? = null
    private var storageRef: StorageReference? = null
    private var mAuth: FirebaseAuth? = null
    var ad: User? = null
    var user: FirebaseUser? = null
    var record = MediaRecorder()
    var isRecording = false
    var userRole: String? = null
    //var player = MediaPlayer()
    var anyEdits: Boolean = false
    var existReportTpye: String? = null

    var reportAdapter: AddedReportListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_report)
        setSupportActionBar(toolbar)

        database = FirebaseDatabase.getInstance()
        mAuth = FirebaseAuth.getInstance()
        user = mAuth?.currentUser

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)

        val data = intent.extras
        locKey = data.getString("key")
        authKey = data.getString("authKey")
        reportKey = data.getString("reportKey")
        userRole = data.getString("userRole")
        anyEdits = false
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        locImg = findViewById(R.id.locImg)
        name = findViewById(R.id.locName)
        address = findViewById(R.id.addressLine)
        progBar = findViewById(R.id.homeprogress)
        currDate = findViewById(R.id.currDate)
        reportTypeSpinner = findViewById(R.id.newReportType)
        takePic = findViewById(R.id.takePic)
        takeAudio = findViewById(R.id.takeAudio)
        takeNote =findViewById(R.id.takeNote)
        takeForm = findViewById(R.id.takeForm)
        takeFromGallery = findViewById(R.id.addFromGallery)
        repTitle = findViewById(R.id.reportTitle)
        reportItemList = findViewById(R.id.reportItems)

        if(data.getString("date")!=null){
            pastDateAddReport = true
            date = data.getString("date")
            val currDate = SimpleDateFormat("MMMM dd, yyyy").parse(date)
            val dbDateFormat = SimpleDateFormat("dd-MM-yyyy")

            dateDB = dbDateFormat.format(currDate)
            val fullDateFormat = SimpleDateFormat("MMMM dd, yyyy")
            date = fullDateFormat.format(currDate).toString()

        }else{
            date = SimpleDateFormat("MMMM dd, yyyy").format(Calendar.getInstance().time)
            dateDB = SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().time)
        }

        if(reportKey!=null){
            if(intent.hasExtra("date")){
                editMode = true
                date = data.getString("date")
                existReportTpye = data.getString("reportType")
                val currDate = SimpleDateFormat("MMMM dd, yyyy").parse(date)
                val dbDateFormat = SimpleDateFormat("dd-MM-yyyy")
                dateDB = dbDateFormat.format(currDate)
                val fullDateFormat = SimpleDateFormat("MMMM dd, yyyy")
                date = fullDateFormat.format(currDate).toString()
            }

        }else{
            reportKey = database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(date)?.push()?.key//key for new report
        }

        if(intent.hasExtra("datafile")){


        }

        if(userRole?.toLowerCase() == "basic"  && editMode){
            reportTypeSpinner?.isClickable = false
            reportTypeSpinner?.isEnabled = false
            repTitle?.isClickable = false
            repTitle?.isEnabled = false
        }

        currDate?.text = date

        var allPermissions  = arrayOf(android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        var i =1
        for(perm in allPermissions){
            if (ContextCompat.checkSelfPermission(this@NewReport, perm)//checking permissions
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                ActivityCompat.requestPermissions(this@NewReport,
                       allPermissions,
                        i)

            }
            i++
        }


        locationTabView.setOnClickListener {
            startActivity(Intent(this@NewReport, MainActivity::class.java))
        }
        val intent = Intent(this@NewReport, MainActivity::class.java)
        notesTabView.setOnClickListener {
            intent.putExtra("tabSel",1)
            startActivity(intent)
        }
        settingsTabView.setOnClickListener {
            intent.putExtra("tabSel",2)
            startActivity(intent)
        }

        fab.setOnClickListener { view ->
            saveNewReportItems()
        }



        takePic?.setOnClickListener { view ->   //take picture button click listener
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                val imgFile = createImageFile()//create placeholder image file
                photoURI = FileProvider.getUriForFile(this@NewReport,
                        "capturesolution.capturesolutions.fileprovider",
                        imgFile)//uri from file
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                photoURIlist?.add(photoURI!!)//add photo uri
                photoDataFile = DataFile()
                photoDataFile?.uri = Uri.fromFile(imgFile)
                photoDataFile?.type="Photo"
                photoDataFile?.username = ad?.Name
                //Log.d("Photo Uri diff", photoURI.toString() + " NEXT :  " + Uri.fromFile(imgFile))
            }
        }

        takeFromGallery?.setOnClickListener {
            val takefromGallery = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            takefromGallery.type = "image/*"
            startActivityForResult(takefromGallery, IMAGE_PICK_CODE)
            photoDataFile = DataFile()

            photoDataFile?.type="Photo"
            photoDataFile?.username = ad?.Name
        }
        takeAudio?.setOnClickListener {
            val audioFile = createAudioFile()
            val builder = AlertDialog.Builder(this@NewReport)
            // Get the layout inflater
            val inflater = this@NewReport.layoutInflater
            val view = inflater.inflate(R.layout.record_dialog,null)
            builder.setView(view)

            val recordTog = view.findViewById<ToggleButton>(R.id.recordButton)
            record = MediaRecorder()
            record.setAudioSource(MediaRecorder.AudioSource.MIC)
            record.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            record.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            record.setOutputFile(audioFile.absolutePath)
            record.prepare()

            val scaleAnimation = ScaleAnimation(1f, 1.5f, 1f, 1.5f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f)
            scaleAnimation.duration = 1000
            val bounceInterpolator = LinearInterpolator()
            scaleAnimation.interpolator=bounceInterpolator
            scaleAnimation.repeatCount = -1

            recordTog.setOnCheckedChangeListener(object : View.OnClickListener, CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                    if(p1){
                        //start animation
                        isRecording = true
                        p0?.startAnimation(scaleAnimation)
                        record.start()
                    }else{
                        record.stop()
                        p0?.clearAnimation()
                        isRecording = false
                    }
                }
                override fun onClick(p0: View?) {
                }
            })
            builder.setPositiveButton("Save",object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    //save audio file, uri to uri list
                    //make sure recording is stopped
                    if(isRecording){
                        record.stop()
                    }
                    val audioDataFile = DataFile()

                    audioDataFile.uri = Uri.fromFile(audioFile)
                    audioDataFile.type="Audio"
                    audioDataFile.username = ad?.Name

                    reportItemDataFileList.add(audioDataFile)
                    anyEdits = true
                    if(reportAdapter == null) {
                        reportAdapter = AddedReportListAdapter(this@NewReport, userRole!!, editMode, reportItemDataFileList)
                        reportItemList?.adapter = reportAdapter
                    }else{
                        reportAdapter?.notifyDataSetChanged()
                    }
                }
            })
            builder.setNegativeButton("Cancel",object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    p0?.cancel()
                    record.release()
                }
            })
            builder.create().show()
        }
        takeNote?.setOnClickListener {
            val builder = AlertDialog.Builder(this@NewReport)
            // Get the layout inflater
            val inflater = this@NewReport.layoutInflater
            val view = inflater.inflate(R.layout.dialog_take_note,null)
            builder.setView(view)
            builder.setPositiveButton("Save", object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    val noteTitle = view.findViewById<EditText>(R.id.noteTitle)
                    val noteText = view.findViewById<EditText>(R.id.noteEntry)
                    if(noteTitle.text.isEmpty()) {
                        noteTitle.hint = "Please Enter a Title"
                    }else {
                        //note = Note(Calendar.getInstance().timeInMillis, noteTitle.text.toString(), noteText.text.toString(), "Note")
                        var noteDataFile = DataFile(Calendar.getInstance().timeInMillis/1000, noteTitle.text.toString(), noteText.text.toString(), "Note", ad?.Name!!)
                        reportItemDataFileList.add(noteDataFile)
                        anyEdits = true
                    }
                    if(reportAdapter == null) {
                        reportAdapter = AddedReportListAdapter(this@NewReport, userRole!!, editMode, reportItemDataFileList)
                        reportItemList?.adapter = reportAdapter
                    }else{
                        reportAdapter?.notifyDataSetChanged()
                    }
                }
            })
            builder.setNegativeButton("Cancel",object: DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    p0!!.cancel()
                }
            })
            builder.create().show()
        }
        takeVideo?.setOnClickListener {
            val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                val videoFile = createVideoFile()
                val videoURI = FileProvider.getUriForFile(this@NewReport,
                        "capturesolution.capturesolutions.fileprovider",
                        videoFile)//uri from file

                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI)
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE)
                videoDataFile = DataFile()
                videoDataFile?.uri = Uri.fromFile(videoFile)
                videoDataFile?.type="Video"
                videoDataFile?.username = ad?.Name
            }
        }
        takeForm?.setOnClickListener {

            var formSelector = Intent(this, FormSelector::class.java)
            formSelector.putExtra("authKey", authKey)
            formSelector.putExtra("locKey",locKey)
            formSelector.putExtra("date",dateDB)
            formSelector.putExtra("reportKey",reportKey)

            startActivityForResult(formSelector,10)
        }

        loadDetails()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(grantResults.size ==4){
            if(grantResults[0] == PackageManager.PERMISSION_DENIED) {
                takeAudio?.visibility = View.GONE
                Toast.makeText(this@NewReport,"Recording Functionality Disabled",Toast.LENGTH_LONG).show()
            }
            if(grantResults[1] == PackageManager.PERMISSION_DENIED) {
                takePic?.visibility = View.GONE
                takeVideo?.visibility = View.GONE
                Toast.makeText(this@NewReport,"Camera Functionality Disabled",Toast.LENGTH_LONG).show()
            }
            if(grantResults[2] == PackageManager.PERMISSION_DENIED) {
                takeFromGallery?.visibility = View.GONE
                Toast.makeText(this@NewReport,"Gallery Select Functionality Disabled",Toast.LENGTH_LONG).show()
            }
            if(grantResults[3] == PackageManager.PERMISSION_DENIED) {
                takePic?.visibility = View.GONE
                Toast.makeText(this@NewReport,"Camera Functionality Disabled",Toast.LENGTH_LONG).show()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            reportItemDataFileList.add(photoDataFile!!)
            anyEdits = true
            var exif = ExifInterface(photoDataFile?.uri?.path)
            if(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)==0) {
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, "6")
                exif.saveAttributes()
            }
        }
        if(requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK){
            reportItemDataFileList.add(videoDataFile!!)
            anyEdits = true
        }
        if(requestCode==10 && resultCode == RESULT_OK){
            var dataFile = data?.extras?.getSerializable("datafile") as DataFile

            anyEdits = true
            var existingFile = reportItemDataFileList.find { it.key == dataFile.key }
            if(existingFile==null){
                reportItemDataFileList.add(dataFile)
            }else{
                reportItemDataFileList.remove(existingFile)
                reportItemDataFileList.add(dataFile)
            }

            if(reportAdapter==null) {
                val reportAdapter = AddedReportListAdapter(this@NewReport, userRole!!, editMode, reportItemDataFileList)
                reportItemList?.adapter = reportAdapter
            }else{
                reportAdapter?.notifyDataSetChanged()
            }
        }
        if(requestCode == IMAGE_PICK_CODE && resultCode== Activity.RESULT_OK && data?.data!=null){
            anyEdits = true
            photoDataFile?.uri = data.data



            var galleryContent = contentResolver?.openInputStream(data.data!!)
            galleryContent?.read()

            var imageFile = createImageFile()

            imageFile.copyInputStreamToFile(galleryContent!!)

            photoDataFile?.date = imageFile.lastModified()/1000
            photoDataFile?.size = imageFile.length()

            reportItemDataFileList.add(photoDataFile!!)
        }
        if(reportAdapter==null) {
            val reportAdapter = AddedReportListAdapter(this@NewReport, userRole!!, editMode, reportItemDataFileList)
            reportItemList?.adapter = reportAdapter
        }else{
            reportAdapter?.notifyDataSetChanged()
        }
    }

    fun File.copyInputStreamToFile(inputStream: InputStream) {
        this.outputStream().use { fileOut ->
            inputStream.copyTo(fileOut)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId!!.equals(android.R.id.home)){
            if(anyEdits) {
                loadSaveDialog()
                return true
            }
            if(reportAdapter!=null&& reportAdapter?.getEdted()!!){
                loadSaveDialog()
                return true
            }
            else{
                finish()
            }
        }
        return true
    }

    override fun onBackPressed() {
        if(anyEdits) {
            loadSaveDialog()
        }
        else if(reportAdapter!=null&& reportAdapter?.getEdted()!!){
            loadSaveDialog()
        }
        else{
            finish()
        }
    }

    private fun loadDetails() {
        var reference = database?.getReference("Locations")
        var refUsers = database?.getReference("Users")?.child(authKey)


        var queryLoc = reference?.child(authKey)
        var queryUser = refUsers

        queryLoc?.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                if (p0?.key.equals(locKey)) {
                    var loc = p0?.getValue(Location::class.java)
                    try {
                        Glide.with(locImg?.context).load(loc?.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object : RequestListener<Drawable> {
                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                progBar?.visibility = View.INVISIBLE
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return true
                            }
                        }).into(locImg)
                    }catch (e: IllegalArgumentException){

                    }

                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                }
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                //Log.d("CHILD",p0.toString())

                if (p0?.key.equals(locKey)) {
                    var loc = p0?.getValue(Location::class.java)

                    /*Picasso.get().load(loc?.url).fit().into(locImg, object: Callback {
                        override fun onSuccess() {
                            progBar?.visibility =View.INVISIBLE
                        }

                        override fun onError(e: Exception?) {

                        }
                    })*/
                    if(loc?.url.isNullOrEmpty()){
                        loc?.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                    }
                    try {
                        Glide.with(locImg?.context).load(loc?.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object : RequestListener<Drawable> {
                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                progBar?.visibility = View.INVISIBLE
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return true
                            }
                        }).into(locImg)
                    }catch (e: IllegalArgumentException){

                    }

                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                Toast.makeText(this@NewReport, "Location Does Not Exist", Toast.LENGTH_LONG).show()
                finish()
            }
        })

        queryUser?.addChildEventListener(object:ChildEventListener{//getting userRole object.
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                if(p0?.key == user?.uid){
                    ad = p0?.getValue(User::class.java)
                    userRole = ad?.Role
                }
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                if(p0?.key == user?.uid){
                    ad = p0?.getValue(User::class.java)
                    userRole = ad?.Role
                }

            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                mAuth?.signOut()
                finish()
            }
        })
        getReportTypeAdapter()
    }
    fun getReportTypeAdapter(){//get the report types for the particular location
        var refReport = database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")
        var referenceReportTypes = database?.getReference("ReportTypes")
        var queryRepTypes = referenceReportTypes?.child(authKey)
        queryRepTypes?.addChildEventListener(object : ChildEventListener {

            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                var ax = p0?.getValue(String::class.java)
                if(!reportTypeList!!.contains("Report Type")){
                    reportTypeList?.add("Report Type")
                }
                reportTypeList?.add(ax!!)
                reportTypeList?.sort()
                if(aa == null){
                    aa = ArrayAdapter(this@NewReport, android.R.layout.simple_spinner_item, reportTypeList)
                    aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    reportTypeSpinner?.adapter = aa
                }else{
                    aa?.notifyDataSetChanged()
                }
                reportTypeSpinner?.setSelection(reportTypeList?.indexOf("Report Type")!!)
                if(editMode){
                    reportTypeSpinner?.setSelection(reportTypeList?.indexOf(existReportTpye)!!)
                }

            }

            override fun onChildRemoved(p0: DataSnapshot?) {

                var ax = p0?.getValue(String::class.java)
                if(!reportTypeList!!.contains("Report Type")){
                    reportTypeList?.add("Report Type")
                }
                reportTypeList?.add(ax!!)
                reportTypeList?.sort()
                aa = ArrayAdapter(this@NewReport, android.R.layout.simple_spinner_item, reportTypeList)
                aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                reportTypeSpinner?.adapter = aa
                reportTypeSpinner?.setSelection(aa?.getPosition("Report Type")!!)
                reportTypeSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    }
                }
                reportTypeSpinner?.setSelection(reportTypeList?.indexOf(existReportTpye)!!)

            }
        })

        if(editMode){//if report existing, load existing data into UI elements.
            val currDate = SimpleDateFormat("MMMM dd, yyyy").parse(date)
            val dbDateFormat = SimpleDateFormat("dd-MM-yyyy")
            var queryReport =refReport?.child(dbDateFormat.format(currDate).toString())
            val queryObj = queryReport
            val queryReportObj = queryReport?.child("Objects")
            queryObj?.addChildEventListener(object:ChildEventListener{
                override fun onCancelled(p0: DatabaseError?) {
                }

                override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                    existinReport = p0?.getValue(Report::class.java)
                    repTitle?.setText(existinReport?.reportTitle)

                }

                override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                    if(p0?.key==reportKey){
                        existinReport = p0?.getValue(Report::class.java)
                        repTitle?.setText(existinReport?.reportTitle)//set report title for existing report
                        //set report type for a existing report


                        p0?.child("Objects")?.ref?.addChildEventListener(object:ChildEventListener{
                            override fun onCancelled(p0: DatabaseError?) {
                            }

                            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                            }

                            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {

                                val newDataFile = p0?.getValue(DataFile::class.java)
                                newDataFile?.dbRef = p0?.ref.toString()

                                var oldDataFile = reportItemDataFileList.find { it.key == newDataFile?.key }
                                reportItemDataFileList.remove(oldDataFile)
                                reportItemDataFileList.add(newDataFile!!)

                                reportAdapter?.notifyDataSetChanged()


                            }

                            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                                val dataFile = p0?.getValue(DataFile::class.java)
                                dataFile?.dbRef = p0?.ref.toString()
                                if(!reportItemDataFileList.contains(dataFile)) {
                                    reportItemDataFileList.add(dataFile!!)
                                    if(reportAdapter==null) {
                                        reportAdapter = AddedReportListAdapter(this@NewReport, userRole!!, editMode,reportItemDataFileList)
                                        reportItemList?.adapter = reportAdapter
                                    }else{
                                        reportAdapter?.notifyDataSetChanged()
                                    }

                                }                                    }

                            override fun onChildRemoved(p0: DataSnapshot?) {
                                val removedDataFile = p0?.getValue(DataFile::class.java)
                                reportItemDataFileList.remove(reportItemDataFileList.find { it.key == removedDataFile?.key })
                                reportAdapter?.notifyDataSetChanged()
                            }
                        })

                        /*for(ds in p0?.child("Objects")?.children!!.iterator()){
                            val dataFile = ds?.getValue(DataFile::class.java)
                            dataFile?.dbRef = p0.ref.toString()
                            if(!reportItemDataFileList.contains(dataFile)) {
                                reportItemDataFileList.add(dataFile!!)
                                if(reportAdapter==null) {
                                    reportAdapter = AddedReportListAdapter(this@NewReport, ad!!, editMode,reportItemDataFileList)
                                    reportItemList?.adapter = reportAdapter
                                }else{
                                    reportAdapter?.notifyDataSetChanged()
                                }

                            }
                        }*/
                    }

                }
                override fun onChildRemoved(p0: DataSnapshot?) {
                }
            })//getting the report object


        }
    }

    fun createImageFile(): File {
        var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        var imageFileName = "JPEG_" + timeStamp + "_"
        var storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        var image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = image.getAbsolutePath();

        return image
    }

    fun createAudioFile(): File {
        var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());
        var audioFileName = timeStamp
        var storageDir = getExternalFilesDir(Environment.DIRECTORY_MUSIC)
        var audio = File.createTempFile(
                audioFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        )
        currentAudioName = audioFileName
        currentAudioPath = audio.absolutePath
        return audio
    }

    fun createVideoFile(): File {
        var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());
        var videoFileName = timeStamp
        var storageDir = getExternalFilesDir(Environment.DIRECTORY_MOVIES)
        var video = File.createTempFile(
                videoFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        )

        return video
    }

    fun saveNewReportItems(){
        if(reportTypeSpinner?.selectedItem.toString().equals("Report Type")){
            Toast.makeText(this@NewReport, "Please Select a Report Type", Toast.LENGTH_LONG).show()
        }else {
            //SAVE report
            var key:String?
            var rep:Report?
            if(editMode) {
                key = reportKey//existing report key
                rep = existinReport//report object created with key
                rep?.createdByName = ad?.Name
                rep?.createdDate = Calendar.getInstance().timeInMillis/1000
            }else{
                key = reportKey//new report key, set in OnCreate
                rep = Report(date!!, dateDB!!, repTitle?.text.toString(), reportTypeSpinner?.selectedItem.toString(), key!!, user!!.uid,ad?.Name!!,Calendar.getInstance().timeInMillis/1000, userRole!!)//report object created with key
            }

            database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.setValue(rep)//accessing/creating child reports -> child date -> set value for report object
            //
            if(reportItemList?.adapter!=null) {

                val listviewAdapter: AddedReportListAdapter = reportItemList?.adapter as AddedReportListAdapter

                reportItemDataFileList = listviewAdapter.getList().toMutableList()

                for (reportItem in reportItemDataFileList) {
                    val itemType = reportItem.type//item Type
                    val objKey = database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.child("Objects")?.push()?.key
                    if (!itemType.equals("Note") && itemType!="Form") {//uploading Images and Audio files, not Note file
                        if(reportItem.uri==null){
                            //file is not local, from web
                            var date = reportItem.date
                            val file = DataFile(date!!, reportItem.name!!, reportItem.size!!, reportItem.type!!, reportItem.url!!, ad?.Name!!)//creating data file of audio type with all info
                            database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.child("Objects")?.child(objKey)?.setValue(file)//uploaded DataFile Object, not FILE type
                        }else {
                            val itemFile = File(reportItem.uri?.path)
                            val itemUri = reportItem.uri!!
                            storageRef = FirebaseStorage.getInstance().reference.child(itemType!!).child(locKey!!).child(user!!.uid)
                            val riversRef = storageRef?.child(itemUri.lastPathSegment)//uploading new, local file.
                            val uploadTask = riversRef?.putFile(itemUri)
                            uploadTask?.addOnSuccessListener(object : OnSuccessListener<UploadTask.TaskSnapshot> {
                                override fun onSuccess(p0: UploadTask.TaskSnapshot?) {
                                    riversRef.downloadUrl.addOnSuccessListener(object : OnSuccessListener<Uri> {
                                        override fun onSuccess(p0: Uri?) {
                                            var file: DataFile
                                            if(itemUri.toString().contains("com.google.android.apps.photos")){
                                                file = DataFile(reportItem.date!!, itemFile.name, reportItem.size!!, itemType, p0.toString(), ad?.Name!!)//creating data file of audio type with all info
                                            }else{
                                                file = DataFile(itemFile.lastModified()/1000, itemFile.name, itemFile.length(), itemType, p0.toString(), ad?.Name!!)//creating data file of audio type with all info
                                            }

                                            database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.child("Objects")?.child(objKey)?.setValue(file)//uploaded DataFile Object, not FILE type
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else{
                        if(itemType.equals("Note")){
                            val noteObjKey = database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.child("Objects")?.push()?.key//fetches placeholder in realtime DB for the note information.
                            //reportItem.dbRef =
                            reportItem.key = noteObjKey
                            database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(key)?.child("Objects")?.child(noteObjKey)?.setValue(reportItem)//uploaded Note Object, not FILE type
                        }else{
                            database?.getReference("Locations")?.child(authKey)?.child(locKey)?.child("Reports")?.child(dateDB)?.child(reportKey)?.child("Objects")?.child(reportItem?.key)?.setValue(reportItem)
                                    ?.addOnCompleteListener(object: OnCompleteListener<Void> {
                                        override fun onComplete(p0: Task<Void>) {
                                            finish()
                                        }
                                    })
                        }
                    }
                }
            }
            finish()
        }
    }

      fun loadSaveDialog(){
        val builder = AlertDialog.Builder(this@NewReport)
        // Get the layout inflater
        builder.setTitle("Save Report")
        builder.setMessage("Did you want to save your report?")
        builder.setPositiveButton("Save", object: DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                saveNewReportItems()
            }
        })
        builder.setNegativeButton("No", object: DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                finish()
            }
        })
        builder.create().show()

    }

    class AddedReportListAdapter(private var activity: NewReport, var userRole: String, var isEdit: Boolean, var items: MutableList<DataFile>):BaseAdapter() {
        var mHandler = Handler()
        var edited = false
        var datafiles = items.sortedByDescending { it.date }.toMutableList()


        //isEdit tells if the report is being edited, edited tells if the report has been changed by the userRole deleting something.
        private class imageViewHolder(row: View?){
            var image : ImageView? = null
            var deleteButton: ImageButton? = null
            var dateStamp: TextView? = null
            var itemUser: TextView? = null
            var progBar: ProgressBar? = null
            init{
                this.image = row?.findViewById<ImageView>(R.id.takenImage)
                this.deleteButton = row?.findViewById<ImageButton>(R.id.deleteImage)
                this.dateStamp = row?.findViewById(R.id.dateStamp)
                this.itemUser =row?.findViewById(R.id.itemUser)
                this.progBar = row?.findViewById(R.id.progressBar)
            }
        }
        private class videoViewHolder(row:View?){
            var video: VideoView? = null
            var deleteButton: ImageButton? = null
            var dateStamp: TextView? = null
            var itemUser: TextView? = null
            init{
                this.deleteButton = row?.findViewById<ImageButton>(R.id.deleteImage)
                this.video = row?.findViewById<VideoView>(R.id.takenVideo)
                this.dateStamp = row?.findViewById(R.id.dateStamp)
                this.itemUser =row?.findViewById(R.id.itemUser)
            }

        }

        private class audioViewHolder(row: View?){
            var playPause : ToggleButton? = null
            var seekbar: SeekBar? = null
            var deletButton: ImageButton? = null
            var dateStamp: TextView? = null
            var itemUser: TextView? = null
            init{
                playPause = row?.findViewById(R.id.togglePlay)
                seekbar = row?.findViewById(R.id.seekAudio)
                deletButton = row?.findViewById(R.id.deleteAudio)
                this.dateStamp = row?.findViewById(R.id.dateStamp)
                this.itemUser =row?.findViewById(R.id.itemUser)
            }

        }

        private class noteViewHolder(row: View?){
            var noteTitle : TextView? = null
            var noteText: TextView? = null
            var deleteButton: ImageButton? = null
            var dateStamp: TextView? = null
            var itemUser: TextView? = null
            init{
                this.noteTitle = row?.findViewById(R.id.noteTitle)
                this.noteText = row?.findViewById(R.id.noteContent)
                this.deleteButton = row?.findViewById(R.id.deleteNote)
                this.dateStamp = row?.findViewById(R.id.dateStamp)
                this.itemUser =row?.findViewById(R.id.itemUser)
            }

        }

        private class formViewHolder(row: View?){
            var noteTitle : TextView? = null
            var imgBox: LinearLayout?=null
            var deleteButton: ImageButton? = null
            var dateStamp: TextView? = null
            var itemUser: TextView? = null
            init{
                this.noteTitle = row?.findViewById(R.id.noteTitle)
                this.imgBox = row?.findViewById(R.id.formImgBox)
                this.deleteButton = row?.findViewById(R.id.deleteNote)
                this.dateStamp = row?.findViewById(R.id.dateStamp)
                this.itemUser =row?.findViewById(R.id.itemUser)
            }

        }


        override fun getItemViewType(position: Int): Int {
            datafiles = items.sortedByDescending { it.date }.toMutableList()
            if(datafiles.get(position).type.equals("Photo")) {
                return 1
            }
            if(datafiles.get(position).type.equals("Audio")) {
                return 2
            }else if(datafiles.get(position).type.equals("Note")){
                return 3
            }
            else if(datafiles.get(position).type.equals("Video")){
                return 0
            }else if(datafiles.get(position).type.equals("Form")){
                return 4
            }
            else{
                return 0
            }

        }

        override fun getViewTypeCount(): Int {
            return 5
        }


        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view: View
            val format = SimpleDateFormat("h:mm a MMMM d, yyyy")
            if(getItemViewType(p0)==0){//video files

                val videoDatafile = getItem(p0)as DataFile 
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.video_report_item, null)
                val viewHolder = videoViewHolder(view)
                view?.tag = viewHolder

                var intent = Intent(Intent.ACTION_VIEW)

                if(videoDatafile.uri!=null){
                    viewHolder.video?.setVideoURI(videoDatafile.uri)
                    val file = File(videoDatafile.uri?.path)
                    var timestamp = Date(file.lastModified())
                    viewHolder.video?.setVideoURI(videoDatafile.uri)
                    viewHolder.dateStamp?.text = format.format(timestamp)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    var fileURI = FileProvider.getUriForFile(activity, "capturesolution.capturesolutions.fileprovider",file)


                    intent.setDataAndType(fileURI, "video/*")
                }else{
                    viewHolder.video?.setVideoURI(Uri.parse(videoDatafile.url))
                    var timestamp = Date(videoDatafile.date!! * 1000)
                    viewHolder.dateStamp?.text = format.format(timestamp)
                    intent.setDataAndType(Uri.parse(videoDatafile.url), "video/*")
                }
                viewHolder.deleteButton?.setOnClickListener {

                        datafiles.remove(videoDatafile)
                    items = datafiles
                        edited = true
                        notifyDataSetChanged()
                }

                if(userRole!="admin"){

                        viewHolder.deleteButton?.visibility = View.INVISIBLE

                }

                viewHolder.itemUser?.text = videoDatafile.username

                viewHolder.video?.setOnTouchListener(object: View.OnTouchListener{
                    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                        activity.startActivity(intent)
                        return true
                    }
                })

                return view
            }
            if(getItemViewType(p0)==1){
                val imageDataFile = getItem(p0)as DataFile//get Datafile of type "Image"
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.image_report_item, null)
                val viewHolder = imageViewHolder(view)
                view?.tag = viewHolder
                //loading image into imageview

                //viewHolder.image?.setImageResource(R.drawable.ic_add_a_photo_accent_24dp)
                viewHolder.deleteButton?.setOnClickListener {
                        datafiles.remove(imageDataFile)
                        items = datafiles
                        edited = true
                        notifyDataSetChanged()
                }

                if(userRole!= "admin"){
                    viewHolder.deleteButton?.visibility = View.INVISIBLE
                }

                var intent = Intent()
                viewHolder.itemUser?.text = imageDataFile.username
                //setting timestamp
                var timestamp: Date

                if(imageDataFile.uri!=null) {
                    //if local file
                    Glide.with(activity).load(imageDataFile.uri).apply(RequestOptions.downsampleOf(DownsampleStrategy.AT_LEAST))
                            .listener(object: RequestListener<Drawable>{
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            viewHolder.progBar?.visibility =View.INVISIBLE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            viewHolder.progBar?.visibility =View.INVISIBLE
                            return false
                        }
                    }).into(viewHolder.image!!)

                    val file = File(imageDataFile.uri?.path)
                    timestamp = Date(file.lastModified())

                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                    if(imageDataFile.uri?.toString()!!.contains("com.google.android.apps.photos")){
                        //gallery image
                        timestamp = Date(imageDataFile.date!! * 1000)
                        intent.setDataAndType(imageDataFile.uri, "image/*")
                    }else{
                        var fileURI = FileProvider.getUriForFile(activity, "capturesolution.capturesolutions.fileprovider",file)
                        intent.setDataAndType(fileURI, "image/*")
                    }
                    viewHolder.dateStamp?.text = format.format(timestamp)
                    //

                    //

                }else if(imageDataFile.url!=null){
                    //if online file
                    Glide.with(activity).load(imageDataFile.url).apply(RequestOptions.downsampleOf(DownsampleStrategy.AT_LEAST))
                            .listener(object: RequestListener<Drawable>{
                                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                    viewHolder.progBar?.visibility =View.INVISIBLE
                                    return false
                                }

                                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                    viewHolder.progBar?.visibility =View.INVISIBLE
                                    return false
                                }
                            })
                            .into(viewHolder.image!!)
                    Log.d("TIMESTAMP", imageDataFile.date.toString() )
                    timestamp = Date(imageDataFile.date!! * 1000)

                    viewHolder.dateStamp?.text = format.format(timestamp)
                    intent.setDataAndType(Uri.parse(imageDataFile.url), "image/*")
                }

                viewHolder.image?.setOnClickListener {
                    intent.action = Intent.ACTION_VIEW
                    activity.startActivity(intent)
                }

                return view
            }
            if(getItemViewType(p0)==2){
                val audioDataFile = getItem(p0)as DataFile//getting the datafile obj from the uri
                val player = MediaPlayer()
                if(audioDataFile.uri!=null) {
                    val audioFile = File(audioDataFile.uri?.path)//getting the file from the uri
                    player.setDataSource(audioFile.absolutePath)//setting player source to that file path
                }else{
                    //if online file, setting player to online path.
                    player.setDataSource(audioDataFile.url)
                }
                player.prepareAsync()
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.audio_report_item, null)
                val viewHolder = audioViewHolder(view)
                view?.tag = viewHolder
                //do audio loading here, on delete click items
                viewHolder.deletButton?.setOnClickListener {
                    if(userRole=="basic"){

                    }else {
                        datafiles.remove(audioDataFile)
                        items = datafiles
                        notifyDataSetChanged()
                        edited = true
                    }
                }

                if(userRole!="admin"){
                    viewHolder.deletButton?.visibility = View.INVISIBLE
                }
                viewHolder.itemUser?.text = audioDataFile.username
                player.setOnPreparedListener {
                    viewHolder.seekbar?.max = it.duration//seekbar max = audio file duration
                    viewHolder.playPause?.setOnCheckedChangeListener { compoundButton, b ->
                        val runnable = object: Runnable{
                            override fun run() {
                                val mCurrentPosition = player.currentPosition
                                viewHolder.seekbar?.progress = mCurrentPosition
                                mHandler.postDelayed(this, 500)//updating seekbar every 500 ms
                            }
                        }
                        if(b){
                            it.start()
                            mHandler.postDelayed(runnable,500)//start the seekbar updating 1 sec after playing audio

                        }else{
                            it.pause()
                            mHandler.removeCallbacks(runnable)//on pause stop the seekbar movement
                        }
                        viewHolder.seekbar?.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
                            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                                if(p2) {
                                    it.seekTo(p1)
                                }
                            }
                            override fun onStartTrackingTouch(p0: SeekBar?) {
                                it.pause()
                            }
                            override fun onStopTrackingTouch(p0: SeekBar?) {
                                if(b){
                                    it.start()
                                }

                            }
                        })
                    }

                }



                //audio timestamp
                var timestamp: Date

                if(audioDataFile.uri!=null) {
                    //if local file only
                    val file = File(audioDataFile.uri?.path)
                    timestamp = Date(file.lastModified())
                    viewHolder.dateStamp?.text = format.format(timestamp)
                }else{
                    //if online file
                    timestamp = Date(audioDataFile.date!! * 1000)
                    viewHolder.dateStamp?.text = format.format(timestamp)
                }

                return view
            }
            else if(getItemViewType(p0)==3){
                val noteDataFile = getItem(p0)as DataFile//getting DataFile of note type
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.note_report_item, null)
                val viewHolder = noteViewHolder(view)
                view?.tag = viewHolder
                viewHolder.noteText?.text = noteDataFile.note
                viewHolder.noteTitle?.text = noteDataFile.title
                viewHolder.deleteButton?.setOnClickListener {
                    if(userRole=="basic"){

                    }else {
                        datafiles.remove(noteDataFile)
                        items = datafiles
                        edited = true
                        notifyDataSetChanged()
                    }
                }

                viewHolder.noteText?.setOnClickListener {
                    var editNoteItem = Intent(activity,AddNote::class.java)
                    editNoteItem.putExtra("editNoteItemPath",noteDataFile.dbRef)
                    editNoteItem.putExtra("editNoteKey", noteDataFile.key)
                    editNoteItem.putExtra("editNoteTitle",noteDataFile.title)
                    editNoteItem.putExtra("editNoteContent",noteDataFile.note)

                    startActivity(activity,editNoteItem, null)
                }

                if(userRole!="admin"){
                    viewHolder.deleteButton?.visibility = View.GONE
                }

                viewHolder.itemUser?.text = noteDataFile.username
                //do note loading here, on delete click items
                //note timestamp
                if(noteDataFile.date!=null){
                    var timestamp = noteDataFile.date!! * 1000
                    viewHolder.dateStamp?.text = format.format(timestamp)
                }

                return view
            }
            else if(getItemViewType(p0)==4){
                //data objects
                val formDataFile = getItem(p0)as DataFile//getting DataFile of note type
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.form_report_item, null)
                val viewHolder = formViewHolder(view)
                view?.tag = viewHolder
                viewHolder.noteTitle?.text = formDataFile.name

                var timestamp = formDataFile.date!! * 1000

                viewHolder.dateStamp?.text = format.format(timestamp)
                viewHolder.itemUser?.text = formDataFile.username

                if(userRole!="admin"){
                    viewHolder.deleteButton?.visibility = View.GONE
                }

                viewHolder.deleteButton?.setOnClickListener {
                    if(userRole=="basic"){

                    }else {
                        datafiles.remove(formDataFile)
                        items = datafiles
                        edited = true
                        notifyDataSetChanged()
                    }
                }

                viewHolder.imgBox?.setOnClickListener {
                    var formFiller = Intent(activity,FormFiller::class.java)
                    formFiller.putExtra("formData", formDataFile.data)
                    formFiller.putExtra("formItemPath",formDataFile.dbRef)
                    formFiller.putExtra("formKey", formDataFile.key)
                    formFiller.putExtra("formTemplateName", formDataFile.name)
                    formFiller.putExtra("isFormEdit", true)
                    startActivityForResult(activity,formFiller,10,null)
                }

                return view
            }
            else{//default view
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.note_report_item, null)
                val viewHolder = noteViewHolder(view)
                view?.tag = viewHolder
                return view
            }


        }

        override fun getItem(p0: Int): Any {

            //return items.toMutableList().sortedByDescending { it.date }[p0]
            return datafiles[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return items.size
        }

        fun getList():List<DataFile>{
            return this.items
        }

        fun getEdted(): Boolean{
            return this.edited
        }
    }

    interface EditReportData{
        fun returnReport():Report?
        fun returnLocObjects():List<DataFile>?
    }
}
