package capturesolution.capturesolutions.Activities

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*

import capturesolution.capturesolutions.Objects.Location
import capturesolution.capturesolutions.Objects.Report
import capturesolution.capturesolutions.R
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask


import kotlinx.android.synthetic.main.activity_site_details.*
import kotlinx.android.synthetic.main.content_site_details.*
import kotlinx.android.synthetic.main.tab_location.*
import kotlinx.android.synthetic.main.tab_notes.*
import kotlinx.android.synthetic.main.tab_settings.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class LocationDetails : AppCompatActivity() {

    private var database: FirebaseDatabase? = null
    private var auth: FirebaseAuth? = null
    private var locDetailGlide: RequestManager? = null

    var locImg: ImageView? = null
    var progBar: ProgressBar? = null
    var address: TextView? = null
    var name: TextView? = null
    var edit: Button? = null
    var newReport: Button? = null
    var locImgAdd: String? = null
    var loc: Location? = null


    var authKey: String? = null
    var locKey: String? = null
    var userRole: String? = null

    val REQUEST_IMAGE_CAPTURE = 1
    val GET_FROM_GALLERY = 2
    var photoURI: Uri? = null
    var mCurrentPhotoPath: String? = null
    var imgFile: File? = null
    var locImgEdit : ImageView? = null
    var favloc: ToggleButton? = null
    var reportsList: MutableList<EventDay> = mutableListOf()
    var isNewFav = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_site_details)
        setSupportActionBar(toolbar)
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        locDetailGlide = Glide.with(this@LocationDetails)

        var data = intent.extras
        locKey = data.getString("key")
        authKey = data.getString("authKey")

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        locImg = findViewById(R.id.locImg)
        name = findViewById(R.id.locName)
        address = findViewById(R.id.locAddress)
        edit = findViewById(R.id.editLocDetails)
        newReport = findViewById(R.id.newReport)
        favloc= findViewById(R.id.favButton)

        //progBar = findViewById
        progressBar4?.visibility = View.VISIBLE

        getCredentials(authKey!!)
        locationTabView.setOnClickListener {
            startActivity(Intent(this@LocationDetails, MainActivity::class.java))
        }
        val intent = Intent(this@LocationDetails, MainActivity::class.java)
        notesTabView.setOnClickListener {
            intent.putExtra("tabSel",1)
            startActivity(intent)
        }
        settingsTabView.setOnClickListener {
            intent.putExtra("tabSel",2)
            startActivity(intent)
        }
        calendarView.setDate(Calendar.getInstance())
        edit?.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val builder = AlertDialog.Builder(this@LocationDetails)
                // Get the layout inflater
                val inflater = this@LocationDetails.layoutInflater
                val view = inflater.inflate(R.layout.dialog_edit_location,null)
                builder.setView(view)
                //dialog box views
                locImgEdit = view.findViewById<ImageView>(R.id.locImg)
                val picButton = view.findViewById<Button>(R.id.takeLocPic)
                val nameText = view.findViewById<EditText>(R.id.editName)
                val addText = view.findViewById<EditText>(R.id.editStreetAddress)
                val cityText = view.findViewById<EditText>(R.id.newCity)
                val stateText = view.findViewById<EditText>(R.id.newState)
                val zipText = view.findViewById<EditText>(R.id.newZip)
                val galleryUpload = view.findViewById<ImageButton>(R.id.uploadGallery)
                //assign current date into EditText, image into ImageView

                locDetailGlide!!.load(locImgAdd).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).into(locImgEdit!!)
                nameText.setText(loc?.name)
                addText.setText(loc?.address)
                cityText.setText(loc?.city)
                stateText.setText(loc?.state)
                zipText.setText(loc?.zip)
                //when userRole clicks new pic button
                picButton.setOnClickListener {
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        imgFile = createImageFile()//create placeholder image file
                        photoURI = FileProvider.getUriForFile(this@LocationDetails,
                                "capturesolution.capturesolution.fileprovider",
                                imgFile!!)//uri from file
                        photoURI = Uri.fromFile(imgFile)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    }
                }
                //uploading image from gallery
                galleryUpload.setOnClickListener {
                    if (ContextCompat.checkSelfPermission(this@LocationDetails,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(this@LocationDetails,
                                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                                1)
                    }else{
                        startActivityForResult(Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY)
                    }
                }

                builder.setPositiveButton("Save", null)
                builder.setNegativeButton("Cancel", null)
                var alertDialog = builder.create()
                alertDialog.setOnShowListener(object: DialogInterface.OnShowListener {
                    override fun onShow(p0: DialogInterface?) {
                        var positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        var negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                        positiveButton.setOnClickListener {
                            val newName = nameText.text.toString()
                            val newAddr = addText.text.toString()
                            val newState = stateText.text.toString()
                            val newCity = cityText.text.toString()
                            val newZip = zipText.text.toString()

                            if(newName.isEmpty()){
                                Toast.makeText(applicationContext, "Location Not Saved. Please Fill All Fields", Toast.LENGTH_SHORT).show()
                            }else {
                                //reference for location object being edited
                                val locRef = database?.getReference("Locations")?.child(authKey)?.child(locKey)
                                //set new values to each field, regardless of change or not
                                locRef?.child("name")?.setValue(newName)
                                locRef?.child("address")?.setValue(newAddr)
                                locRef?.child("state")?.setValue(newState)
                                locRef?.child("city")?.setValue(newCity)
                                locRef?.child("zip")?.setValue(newZip)
                                //if userRole has taken a new picture, upload it
                                if(photoURI!=null){

                                    val storageRef = FirebaseStorage.getInstance().reference.child("LocationPhotos").child(authKey!!)//LocationPhotos within Firebase Storage
                                    val riversRef = storageRef.child(photoURI!!.lastPathSegment)//storage ref for new uploaded image
                                    val uploadTask = riversRef.putFile(photoURI!!)//uploading image file

                                    uploadTask.addOnSuccessListener(object : OnSuccessListener<UploadTask.TaskSnapshot> {
                                        override fun onSuccess(p0: UploadTask.TaskSnapshot?) {
                                            riversRef.downloadUrl.addOnSuccessListener(object : OnSuccessListener<Uri> {
                                                //gettting download url for image from firebase storage
                                                override fun onSuccess(p0: Uri?) {
                                                    locRef?.child("url")?.setValue(p0.toString())//set download url for new location image.
                                                }
                                            })

                                        }
                                    })
                                }
                                loadingImageProg.visibility = View.VISIBLE
                                alertDialog.dismiss()
                            }
                        }
                        negativeButton.setOnClickListener {
                            if(photoURI?.path!=null) {
                                var file = File(photoURI?.path)
                                file.delete()
                            }
                            p0!!.cancel()
                            alertDialog.dismiss()
                        }
                    }
                })
                alertDialog.show()
            }
        })
        newReport?.visibility = View.INVISIBLE
        /*newReport?.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val intent = Intent(this@LocationDetails, NewReport::class.java)
                intent.putExtra("key",locKey)
                intent.putExtra("authKey",authKey)
                intent.putExtra("userRole", userRole)
                startActivity(intent)
            }
        })*/
        calendarView?.setOnDayClickListener(object: OnDayClickListener{
            override fun onDayClick(eventDay: EventDay?) {
                var clickedDayCalendar = eventDay?.calendar
                var time = clickedDayCalendar?.time
                var format = SimpleDateFormat("MMMM dd, yyyy")
                var date = format.format(time)

                if (time!!.time > System.currentTimeMillis()) {
                    Toast.makeText(this@LocationDetails, "Please Select a Past Date", Toast.LENGTH_LONG).show()
                } else {
                    if (reportsList.contains(eventDay)) {
                        val intent = Intent(this@LocationDetails, DateDetails::class.java)
                        intent.putExtra("date", date)
                        intent.putExtra("key", locKey)
                        intent.putExtra("authKey", authKey)
                        intent.putExtra("userRole", userRole)
                        startActivity(intent)
                    } else {
                        val builder = AlertDialog.Builder(this@LocationDetails)
                        builder.setTitle("Create New Report")
                        builder.setMessage("Would you like to add a new report to " + format.format(eventDay?.calendar?.time) + "?")
                        builder.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                                val intent = Intent(this@LocationDetails, NewReport::class.java)
                                intent.putExtra("date", date)
                                intent.putExtra("key", locKey)
                                intent.putExtra("authKey", authKey)
                                intent.putExtra("userRole", userRole)
                                startActivity(intent)
                            }
                        }).setNegativeButton("No", object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                            }
                        })
                        builder.create().show()
                    }
                }
            }
        })

        loadDetails()
        getLocFav()



    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode==1){
            if((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                //permissions granted
                startActivityForResult(Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY)
            }else{
                Toast.makeText(this@LocationDetails,"Cannot Load Images from Gallery",Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun loadDetails(){

        val reference = database?.getReference("Locations")
        val queryLoc = reference?.child(authKey)

        queryLoc?.addChildEventListener(object: ChildEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                if(p0?.key.equals(locKey)){
                    loc = p0?.getValue(Location::class.java)
                    if(loc?.url.isNullOrEmpty()){
                        locImgAdd = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                    }
                    if(photoURI!=null){
                        locImgAdd = photoURI?.toString()
                    }
                    else{
                        locImgAdd = loc?.url
                    }


                    locDetailGlide!!.load(locImgAdd).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object: RequestListener<Drawable>{
                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            loadingImageProg.visibility =View.INVISIBLE
                            return false
                        }

                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            loadingImageProg.visibility =View.INVISIBLE
                            return true
                        }
                    }).into(locImg)

                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                }
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                if(p0?.key.equals(locKey)){
                    loc = p0?.getValue(Location::class.java)
                    if(loc?.url.isNullOrEmpty()){
                        loc?.url = "https://firebasestorage.googleapis.com/v0/b/capture-a5ed8.appspot.com/o/LocationPhotos%2Fphotos2-512.png?alt=media&token=417bf737-7bbf-4ec1-81dd-cc892f732481"
                    }
                    if(photoURI!=null){
                        loc?.url = photoURI?.toString()
                    }else{
                        locImgAdd = loc?.url
                    }

                    locDetailGlide!!.load(loc?.url).apply(RequestOptions().downsample(DownsampleStrategy.AT_LEAST)).listener(object: RequestListener<Drawable>{
                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            loadingImageProg.visibility =View.INVISIBLE
                            return false
                        }

                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            loadingImageProg.visibility =View.INVISIBLE
                            /*Picasso.get().load(locImgAdd).into(locImg, object: Callback {
                                override fun onSuccess() {

                                }

                                override fun onError(e: Exception?) {

                                }
                            })*/
                            return true
                        }
                    }).into(locImg)

                    /**/

                    name?.text = loc?.name
                    if (loc?.address.isNullOrBlank() && loc?.city.isNullOrBlank()) {
                        address?.text = loc?.state + " " + loc?.zip
                    }
                    if (loc?.state.isNullOrBlank() && loc?.zip.isNullOrBlank()) {
                        address?.text = loc?.address + " " + loc?.city
                    }
                    else {
                        address?.text = loc?.address + " " + loc?.city + ", " + loc?.state + " " + loc?.zip
                    }
                    progressBar4?.visibility = View.INVISIBLE
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                //Location removed
                Toast.makeText(this@LocationDetails, "Location Does Not Exist", Toast.LENGTH_LONG).show()
                finish()

            }
        })

        val queryRep = queryLoc?.child(locKey)?.child("Reports")

        queryRep?.addChildEventListener(object:ChildEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                if(p0?.key!=null && p0.key != "(null)") {
                    val date = p0.key
                    val cal = Calendar.getInstance()
                    calendarView.setDate(cal.time)
                    val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
                    cal?.time = format.parse(date!!)

                    for (rep in p0.children?.iterator()!!) {
                        val report = rep.getValue(Report::class.java)!!
                        if (userRole?.toLowerCase() == "basic" && report.createdById == auth?.currentUser?.uid) {
                            reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))
                            //calendarView.setEvents(reportsList)x
                        }
                        if (userRole?.toLowerCase() == "manager" && report.createdByRole != "admin") {
                            if (report.createdByRole == "manager" && report.createdById == auth?.currentUser?.uid) {
                                reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))
                            } else if (report.createdByRole == "basic") {
                                reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))
                            }
                        } else if (userRole?.toLowerCase() == "admin") {
                            reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))
                        }
                    }
                    calendarView.setEvents(reportsList)
                }
                /*if(userRole?.toLowerCase() == "manager" && (report?.createdByRole == "basic" )){
                    var date = p0?.key

                    cal.time = format.parse(date)

                    calendarView.setDate(Calendar.getInstance().time)
                    reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))

                }
                else if(userRole == "admin"){
                    if(report?.createdByRole== "basic" || report?.createdByRole=="manager" || report?.createdById == auth?.currentUser?.uid) {
                        var date = p0?.key

                        cal.time = format.parse(date)

                        calendarView.setDate(Calendar.getInstance().time)
                        reportsList.add(EventDay(cal, R.drawable.ic_report_black_24dp))
                        //calendarView.setEvents(reportsList)
                    }else {

                    }
                }*/


            }

            override fun onChildRemoved(p0: DataSnapshot?) {
            }
        })
        //calendarView.setEvents()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId!!.equals(android.R.id.home)){
            finish()
            return true
        }
        if(item.itemId==R.id.add_new){
            val intent = Intent(this@LocationDetails, NewReport::class.java)
            intent.putExtra("key",locKey)
            intent.putExtra("authKey",authKey)
            intent.putExtra("userRole", userRole)
            startActivity(intent)
        }
        return true
    }

    fun createImageFile(): File {
        var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());
        var imageFileName = "JPEG_" + timeStamp + "_";
        var storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        var image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            photoURI = Uri.fromFile(imgFile)
            val exif = ExifInterface(Uri.fromFile(imgFile)?.path)
            Log.d("orientation", exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0).toString())
            if(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)==0) {
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, "6")
                exif.saveAttributes()
            }

            locDetailGlide!!.load(photoURI).into(locImgEdit)

        }
        if(requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK){
            var selectedImage = data?.data
            photoURI = selectedImage

            locDetailGlide!!.load(selectedImage).into(locImgEdit)
        }
    }

    fun getCredentials(authKey: String){

        var user = auth?.currentUser
        val userRef = database?.getReference("Users")

        var queryUser = userRef?.child(authKey)?.child(user?.uid)
        queryUser?.addChildEventListener(object: ChildEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                setUserRole(p0!!)
            }
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                setUserRole(p0!!)
            }

            override fun onChildRemoved(p0: DataSnapshot?) {

            }
        })
    }

    fun getLocFav(){
        val favQuery = database?.getReference("Users")?.child(authKey)?.child(auth?.currentUser?.uid)?.child("Favorites")
        favQuery?.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(p0: DataSnapshot?) {
                for(p1 in p0?.children!!.iterator()){
                    if(p1.value == loc?.key){
                        isNewFav = false
                        favloc?.isChecked = true

                    }else{
                        isNewFav = true
                    }
                }
            }
        })
        favloc?.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked && isNewFav){
                database?.getReference("Users")?.child(authKey)?.child(auth?.currentUser?.uid)?.child("Favorites")?.push()?.setValue(loc?.key)
            }
            else if(!isChecked){

                unFavLoc()
            }
        }
    }

    fun unFavLoc(){
        val favQuery = database?.getReference("Users")?.child(authKey)?.child(auth?.currentUser?.uid)?.child("Favorites")

        favQuery?.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                for(p1 in p0?.children!!.iterator()){
                    if(p1.value == loc?.key && !favloc!!.isChecked){
                        p1.ref.removeValue()
                        isNewFav =true
                    }
                }
            }
        })

    }

    fun setUserRole(p0: DataSnapshot?){
        if(p0?.key?.toLowerCase().equals("role")) {
            userRole = p0?.value.toString()
            //Log.d("Role", p0?.value.toString())
            if(userRole=="basic"){
                edit?.visibility = View.INVISIBLE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }



}
