package capturesolution.capturesolutions.Activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import capturesolution.capturesolutions.Objects.Form
import capturesolution.capturesolutions.R
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.tab_location.*
import kotlinx.android.synthetic.main.tab_notes.*
import kotlinx.android.synthetic.main.tab_settings.*

class   FormSelector : AppCompatActivity() {
    var formList: ListView? = null
    var formLoading: ProgressBar? = null

    private var database: FirebaseDatabase? = null
    var formsList: MutableList<Form> = mutableListOf()
    var formAdapter: FormListAdapter? = null
    var authKey: String? =null
    var locKey =""
    var date = ""
    var reportKey = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_selector)
        formList = findViewById(R.id.formList)
        formLoading = findViewById(R.id.listLoadingBar)
        database = FirebaseDatabase.getInstance()
        centerTitle()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Forms"


        authKey = intent.getStringExtra("authKey")
        locKey = intent.getStringExtra("locKey")
        date = intent.getStringExtra("date")
        reportKey = intent.getStringExtra("reportKey")

        val formRef = database?.getReference("FormTemplates")
        val queryForm = formRef?.child(authKey)

        queryForm?.addChildEventListener(object: ChildEventListener{
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                var form = p0?.getValue(Form::class.java)
                //Log.d("Form",form?.name)
                formsList.add(form!!)
                //Log.d("FormCount",formsList.size.toString())
                if(formAdapter==null) {
                    formAdapter = FormListAdapter(this@FormSelector, formsList)
                    formList?.adapter = formAdapter

                }else{
                    formAdapter?.notifyDataSetChanged()

                }
                formLoading?.visibility = View.INVISIBLE
            }
            override fun onChildRemoved(p0: DataSnapshot?) {
            }
            override fun onCancelled(p0: DatabaseError?) {
            }
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

        })

        formList?.setOnItemClickListener { adapterView, view, i, l ->
            val selForm = formAdapter?.getItem(i) as Form
            var startForm = Intent(this,FormFiller::class.java)
            startForm.putExtra("formKey", selForm.key)
            startForm.putExtra("formData", selForm.data)
            startForm.putExtra("authKey", authKey)
            startForm.putExtra("locKey",locKey)
            startForm.putExtra("date",date)
            startForm.putExtra("reportKey",reportKey)
            startForm.putExtra("formTemplateName",selForm.name)

            //startForm.flags = Intent.FLAG_ACTIVITY_FORWARD_RESULT
            startActivityForResult(startForm, 10)
        }

        locationTabView.setOnClickListener {
            startActivity(Intent(this@FormSelector, MainActivity::class.java))
        }
        val intent = Intent(this@FormSelector, MainActivity::class.java)
        notesTabView.setOnClickListener {
            intent.putExtra("tabSel",1)
            startActivity(intent)
        }
        settingsTabView.setOnClickListener {
            intent.putExtra("tabSel",2)
            startActivity(intent)
        }
    }



    private fun centerTitle() {
        val textViews = ArrayList<View>()
        window.decorView.findViewsWithText(textViews, title, View.FIND_VIEWS_WITH_TEXT)
        if (textViews.size > 0) {
            var appCompatTextView: AppCompatTextView? = null
            if (textViews.size == 1)
                appCompatTextView = textViews[0] as AppCompatTextView
            else {
                for (v in textViews) {
                    if (v.parent is Toolbar) {
                        appCompatTextView = v as AppCompatTextView
                        break
                    }
                }
            }
            if (appCompatTextView != null) {
                val params = appCompatTextView.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                appCompatTextView.layoutParams = params
                appCompatTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
                appCompatTextView.setPadding(-100,5,0,5)
            }
        }
    }

    class FormListAdapter(var activity: FormSelector, var list: List<Form>): BaseAdapter(){


        private class FormViewHolder(row:View?){
            var formName: TextView? = null
            init {
                this.formName = row?.findViewById(R.id.formName)
            }
        }


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val form = getItem(position) as Form

            if(convertView==null) {
                val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.formlistrow, null)
            }else{
                view = convertView
            }

            val viewHolder = FormViewHolder(view)
            view.tag = viewHolder
            viewHolder.formName?.text = form.name.toString()

            return view
        }

        override fun getItem(position: Int): Any {
            val sortedForms = list.sortedBy { it.name }
            val sortedFormsRO: List<Form> = sortedForms
            return sortedFormsRO[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId!!.equals(android.R.id.home)) {
            finish()
            return true
        }else{
            return true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 10){
            if(data!=null) {
                setResult(Activity.RESULT_OK, data)
                finish()
            }

        }
    }
}
