package capturesolution.capturesolutions.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import capturesolution.capturesolutions.Fragments.RegisterFragment
import capturesolution.capturesolutions.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_selector.*

class Selector : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selector)

        supportActionBar?.hide()

        var loginLogo = findViewById<ImageView>(R.id.loginlogo)
        var loginButton = findViewById<Button>(R.id.loginButton)
        var forgotPassTextView = findViewById<TextView>(R.id.forgotPassButton)

        Picasso.get().load(R.drawable.capture_login_logo).resize(700, 700).into(loginLogo)

        loginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        forgotPassTextView.setOnClickListener {
            val regFragment = RegisterFragment()
            main_screen.visibility = View.GONE
            addFragment(regFragment, R.id.main_screen_frame_frag)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack("aa")
        fragmentTransaction.commit()
    }

    fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int){
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }

}
