package capturesolution.capturesolutions.Activities

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import android.widget.*
import capturesolution.capturesolutions.Objects.User
import capturesolution.capturesolutions.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_setting_item_list.*
import java.security.SecureRandom


import java.net.HttpURLConnection
import java.net.URL


class SettingItemList : AppCompatActivity() {

    private var listType: String? = null
    private var mAuth: FirebaseAuth? = null
    private var authKey : String? = null
    private var userRole: String? = null
    var database: FirebaseDatabase? = null
    var userList: MutableList<User>? = mutableListOf<User>()
    var repTypeList: MutableList<String>? = mutableListOf<String>()
    var emailList: MutableList<String>? = mutableListOf<String>()
    var usersNameList: MutableList<String>? = mutableListOf<String>()
    var aa: UserListAdapter? = null
    var repAdapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_item_list)

        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listType = intent.getStringExtra("type")
        authKey = intent.getStringExtra("authKey")
        userRole = intent.getStringExtra("userRole")

        if(listType == "userList"){
            supportActionBar?.title = "Users"
            getUsers()
        }
        if(listType == "repTypeList"){
            supportActionBar?.title = "Report Types"
            getReportTypes()
        }

        settingItemList?.setOnItemClickListener { adapterView, view, i, l ->
            if(listType == "userList" && userRole == "admin"){
                val dialog = AlertDialog.Builder(this@SettingItemList)
                val selectedUser = userList?.get(i)
                dialog.setTitle("Change Role for " + selectedUser?.Name + ": ")
                //dialog.setMessage("Pick a role for "+selectedUser?.Name + ": "+ selectedUser?.Email)

                val arrayAdapter = ArrayAdapter<String>(this@SettingItemList, R.layout.user_role_dialog_list)
                arrayAdapter.add("Admin")
                arrayAdapter.add("Basic")
                arrayAdapter.add("Manager")

                dialog.setAdapter(arrayAdapter, object: DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {

                        if(arrayAdapter.getItem(p1) == selectedUser?.Role){

                        }else{
                            selectedUser?.Role = arrayAdapter.getItem(p1).toLowerCase()
                            selectedUser?.Key = authKey
                            database?.reference?.child("Users")?.child(authKey)?.child(selectedUser?.id)?.setValue(selectedUser)
                            aa?.notifyDataSetChanged()
                        }
                    }
                })

                dialog.setNegativeButton("Cancel",object: DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                    }
                })
                dialog.create().show()
            }
            if(listType == "repTypeList" && userRole == "admin"){
                var dialog = AlertDialog.Builder(this@SettingItemList)
                dialog.setTitle("Delete Report Type:")
                dialog.setMessage(repTypeList?.get(i))
                dialog.setPositiveButton("Delete",object: DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        deleteRepTye(repTypeList!![i])
                        repTypeList?.removeAt(i)
                        repAdapter?.notifyDataSetChanged()
                    }
                })
                dialog.setNegativeButton("Cancel",object: DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {

                    }
                })
                dialog.create().show()
            }
            true
        }

        if(listType=="userList" && userRole=="admin") {
            settingItemList?.setOnItemLongClickListener { adapterView, view, i, l ->
                val dialog = AlertDialog.Builder(this@SettingItemList)
                val delUser = userList!![i]
                if(delUser.id==mAuth?.currentUser?.uid){
                    dialog.setTitle("Cannot Delete Own User!")
                    dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, p1: Int) {

                        }
                    })
                }else{
                    dialog.setTitle("Delete User?")
                    dialog.setMessage(delUser.Name + ": " + delUser.Email)
                    dialog.setPositiveButton("Delete", object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            userList?.removeAt(i)
                            aa?.notifyDataSetChanged()
                            delUser.Role = "deleted"
                            database?.reference?.child("Users")?.child(authKey)?.child(delUser.id)?.removeValue()

                            class deleteUser: AsyncTask<Void, Void, String>(){


                                override fun doInBackground(vararg p0: Void?): String {
                                    val registerEndpoint = URL("https://capture-a5ed8.firebaseapp.com/deleteUser?key="+delUser.id)
                                    val urlConnection = registerEndpoint.openConnection() as HttpURLConnection
                                    urlConnection.requestMethod = "GET"
                                    urlConnection.connect()
                                    Log.d("URL ERROR",urlConnection.responseMessage.toString())
                                    return "done"
                                }


                            }
                            deleteUser().execute()

                            //urlConnection.connect()

                        }
                    })
                    dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {

                            }
                    })
                }

                dialog.create().show()
                true
            }
        }

    }

    fun getUsers(){
        val reference = database?.getReference("Users")
        val queryRef = reference?.child(authKey)

        queryRef?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                var user = p0?.getValue(User::class.java)
                if(user?.Role!= "deleted") {
                    user?.id = p0?.ref?.key
                    userList?.add(user!!)
                    emailList?.add(user?.Email!!)
                    userList?.sortBy { it.Name }
                    if (aa == null) {
                        aa = UserListAdapter(this@SettingItemList, userList!!)
                        settingItemList?.adapter = aa
                    } else {
                        aa?.notifyDataSetChanged()
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                var user = p0?.getValue(User::class.java)
                if(user?.Role!= "deleted") {
                    user?.id = p0?.ref?.key
                    userList?.add(user!!)
                    emailList?.add(user?.Email!!)
                    userList?.sortBy { it.Name }
                    if (aa == null) {
                        aa = UserListAdapter(this@SettingItemList, userList!!)
                        settingItemList?.adapter = aa
                    } else {
                        aa?.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                var user = p0?.getValue(User::class.java)
                userList?.remove(user)
                emailList?.remove(user?.Email)
                aa?.notifyDataSetChanged()

            }
        })

    }
    fun getReportTypes(){
        val reference = database?.getReference("ReportTypes")
        val queryRep = reference?.child(authKey)

        queryRep?.addChildEventListener(object:ChildEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                var ax = p0?.getValue(String::class.java)
                if(!repTypeList?.contains(ax)!!) {
                    repTypeList?.add(ax!!)
                    repTypeList?.sort()
                    if (aa == null) {
                        repAdapter = ArrayAdapter(this@SettingItemList, android.R.layout.simple_list_item_1,repTypeList)
                        settingItemList?.adapter = repAdapter
                    } else {
                        repAdapter?.notifyDataSetChanged()
                        aa?.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
                var ax = p0?.getValue(String::class.java)
                repTypeList?.remove(ax)
                repAdapter?.notifyDataSetChanged()
                aa?.notifyDataSetChanged()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId!!.equals(android.R.id.home)) {
            finish()
        }
        if(item.itemId.equals(R.id.add_new)){
            if(listType == "userList"){
                addNewUser()
            }
            if(listType == "repTypeList"){
                addRepTypeDialog()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }
    fun addRepTypeDialog(){
        val reference = database?.getReference("ReportTypes")
        val queryRep = reference?.child(authKey)
        val dialog = AlertDialog.Builder(this@SettingItemList)
        val view = layoutInflater.inflate(R.layout.add_report_type, null)
        val newReportType = view.findViewById<EditText>(R.id.newReportType)
        dialog.setView(view)
        dialog.setTitle("New Report Type")
        dialog.setPositiveButton("Save", null)
        dialog.setNegativeButton("Cancel",null)
        val alert = dialog.create()
        alert.setOnShowListener {
            val positiveButton = alert.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setOnClickListener {
                if(!repTypeList?.contains(newReportType.text.toString())!!) {
                    repTypeList?.add(newReportType.text.toString())
                    queryRep?.push()?.setValue(newReportType.text.toString())
                    if(!repTypeList?.contains(newReportType.text.toString())!!){
                        repTypeList?.add(newReportType.text.toString())
                        repAdapter?.notifyDataSetChanged()
                    }
                    alert.dismiss()
                }else{
                    Toast.makeText(this, "Report Type Already Exists", Toast.LENGTH_SHORT).show()
                }
            }
        }
        alert.show()
    }
    fun deleteRepTye(repType: String){
        val reference = database?.getReference("ReportTypes")
        val queryRep = reference?.child(authKey)
        queryRep?.addChildEventListener(object: ChildEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                if(p0?.value==repType) {
                    p0.ref.removeValue()
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {
            }
        })
    }
    fun addNewUser(){
        val reference = database?.getReference("Users")
        val queryRep = reference?.child(authKey)
        val dialog = AlertDialog.Builder(this@SettingItemList)
        val view = layoutInflater.inflate(R.layout.admin_add_user, null)
        val newName = view.findViewById<EditText>(R.id.newUserName)
        val newEmail = view.findViewById<EditText>(R.id.newUserEmail)
        val regProgress = view.findViewById<ProgressBar>(R.id.userSignUpProgress)
        dialog.setView(view)
        dialog.setTitle("Add New User")
        dialog.setPositiveButton("Add", null)
        dialog.setNegativeButton("Cancel",null)
        val alert = dialog.create()
        alert.setOnShowListener {
            val positiveButton = alert.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setOnClickListener {
                if(newEmail.text.isEmpty() && newName.text.isEmpty()){
                    Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_SHORT).show()
                }else {
                    if (newEmail.text.toString() in emailList!!) {
                        Toast.makeText(this, "User with this Email Exists", Toast.LENGTH_SHORT).show()
                    }
                    if ("@" in newEmail.text.toString()) {

                        regProgress.visibility = View.VISIBLE
                        newEmail.visibility = View.INVISIBLE
                        newName.visibility = View.INVISIBLE
                        val secRandom = SecureRandom()

                        //BigInteger(130, secRandom).toString(32)
                        mAuth?.createUserWithEmailAndPassword(newEmail.text.toString(),"1234test")
                                ?.addOnCompleteListener {
                                    if(it.isSuccessful){
                                        mAuth?.sendPasswordResetEmail(newEmail.text.toString())

                                        emailList?.add(newEmail.text.toString())
                                        aa?.notifyDataSetChanged()
                                        var uid = mAuth?.currentUser?.uid

                                        val profileUpdates = UserProfileChangeRequest.Builder()
                                                .setDisplayName(authKey).build()
                                        mAuth?.currentUser?.updateProfile(profileUpdates)

                                        var user = User("basic",newEmail.text.toString(),newName.text.toString(),authKey!!)
                                        user.id = uid
                                        database?.reference?.child("Users")?.child(authKey)?.child(uid)?.setValue(user)?.addOnCompleteListener {
                                            if(it.isSuccessful){
                                                mAuth?.signOut()
                                                var settings = PreferenceManager.getDefaultSharedPreferences(this)
                                                var a = settings.getString("User Email", "default")
                                                var b = settings.getString("User Password", "default")
                                                mAuth?.signInWithEmailAndPassword(a,b)
                                                Toast.makeText(this, "Sign Up Successful", Toast.LENGTH_SHORT).show()
                                            }else{
                                                Toast.makeText(this, "Sign Up Failed. Please Try Again", Toast.LENGTH_SHORT).show()
                                            }
                                        }
                                        alert.dismiss()
                                    }else{
                                        Toast.makeText(this, "Sign Up Failed. Please Try Again", Toast.LENGTH_SHORT).show()
                                        regProgress.visibility = View.INVISIBLE
                                        newEmail.visibility = View.VISIBLE
                                        newName.visibility = View.VISIBLE
                                    }
                                }
                    } else {
                        Toast.makeText(this, "Please Enter a Valid Email", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        alert.show()
    }

    class UserListAdapter(private var activity: SettingItemList, var items: MutableList<User>): BaseAdapter(){

        private class UserListViewholder(row: View?){
            var userName: TextView? = null
            var userRole: TextView? = null
            init {
                this.userName = row?.findViewById(R.id.userName)
                this.userRole= row?.findViewById(R.id.userRole)
            }
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view: View
            val viewHolder: UserListViewholder
            val user = getItem(p0) as User
            val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            if(p1 == null) {
                view = inflater.inflate(R.layout.user_list_details, null)
                viewHolder = UserListViewholder(view)
                view?.tag = viewHolder
            }else{
                view = p1
                viewHolder = view.tag as UserListViewholder
            }
            viewHolder.userRole?.text = user.Role
            viewHolder.userName?.text = user.Name

            return view

        }

        override fun getItem(p0: Int): Any {
            return items[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 1
        }

        override fun getCount(): Int {
            return items.size
        }
    }

}
